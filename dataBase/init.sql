DROP DATABASE IF EXISTS dionysus;
CREATE DATABASE dionysus;
\c dionysus
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE Stages
(
	StageID SERIAL PRIMARY KEY NOT NULL,
	StageName VARCHAR(255) NOT NULL UNIQUE,
	Description VARCHAR(255) NOT NULL,
	Clue VARCHAR(255) NOT NULL,
	Solution VARCHAR(255) NOT NULL,
	Longitude float(8) NOT NULL,
	Latitude float(8) NOT NULL,
	Cluster VARCHAR(255)
);


CREATE TABLE Users
(	UserID UUID NOT NULL DEFAULT uuid_generate_v1(),
	UserName VARCHAR(16) NOT NULL UNIQUE,
	Email VARCHAR(255) NOT NULL,
	PasswordHash TEXT NOT NULL,
	AdminFlag BOOLEAN NOT NULL,
	CurrentStage int NOT NULL REFERENCES Stages(StageID),
	PRIMARY KEY (UserID)
);

CREATE TABLE StagesCompleted
(	UserID UUID NOT NULL REFERENCES Users(UserID),
	StageID int NOT NULL REFERENCES Stages(StageID),
	PRIMARY KEY (UserID, StageID)
);

/*Populates stages*/
INSERT INTO Stages (StageID,StageName,Description,Clue,Solution,Longitude,Latitude,Cluster) 
VALUES(DEFAULT, 'Rice', 'Clue description', 'In what year was the Forum opened?', '2012', '-3.533381000000001', '50.73506399999987', 'Forum'),
	(DEFAULT, 'Salt', 'Clue description', 'In what year did the University achieve its full university status?', '1955', '-3.534271000000007', ' 50.7351439999999', 'Forum'),
	(DEFAULT, 'Chilli', 'Clue description', 'What is the cafe in the Harrison building called?', 'The Engine Room', '-3.5326209999999962', '50.73747400000064', 'Harrison'),
	(DEFAULT, 'Soy Sauce', 'Clue description', 'What college is based in the Harrison building?', 'The College of Engineering', '-3.5329409999999983', '50.73732400000059', 'Harrison'),
	(DEFAULT, 'Sake', 'Clue description', 'How many floors does the Queens building have?', '3', '-3.534881000000011', '50.73424399999961', 'Queens'),
	(DEFAULT, 'Egg', 'Clue description', 'Who is the head of the History Department?', 'Professor Richard Toye', '-3.535031000000012', '50.733933999999515', 'Queens'),
	(DEFAULT, 'Dimsum', 'Clue description', 'How many sports clubs are there at the university?', '50', '-3.537451000000028', '50.73770400000071', 'Sports Park'),
	(DEFAULT, 'Chopsticks', 'Clue description', 'What college does Law belong to?', 'The College of Social Sciences', '-3.5316109999999896', '50.73643400000031', 'Amory'),
	(DEFAULT, 'Chicken', 'Clue description', 'How many entrances does Amory Parker Moot lecture theatre have?', '4', '-3.5317409999999905', '50.736174000000226', 'Amory'),
	(DEFAULT, 'Final Clue', 'Clue description', 'What was the Univeritys name changed to in 1900?', 'Royal Albert Memorial College', '0', '0','0');
