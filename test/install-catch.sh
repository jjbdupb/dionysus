git clone https://github.com/catchorg/Catch2.git --depth 1
cd Catch2
mkdir build
cd build
cmake ..
make -j4 install
cd /opt/
