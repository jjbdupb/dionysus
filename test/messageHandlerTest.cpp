#define CATCH_CONFIG_MAIN
#define REQUIRE
#define CHECK
#include <catch2/catch.hpp>
#include <pqxx/pqxx>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <fmt/format.h>
#include "../controller/MessageHandler.hpp"
extern const std::string TESTCONF = "TestDionysus.conf";


/**
 * @brief Tests arguments are unpacked correctly
 */
TEST_CASE( "Arguments are unpacked correctly", "[getArgs]" )
{
    std::vector<std::string> testVector(2);
    testVector[0] = "aaa";
    testVector[1] = "bbb";

    REQUIRE( getArgs("aaa|bbb") ==  testVector);
}

/**
 * @brief Tests functions using the database
 */
TEST_CASE ( "Test Database Functions")
{
    /**
    * @brief Tests connection to the database
    */
    SECTION ( "Connect to database")
    {
        pqxx::connection *db = nullptr;
        std::string dbName, user, password, host, port, timeout;
        int maxconnect, attempts, waitTime;
        
        boost::property_tree::ptree pt;
        boost::property_tree::ini_parser::read_ini(TESTCONF, pt);
        dbName = pt.get<std::string>("Database.dbName");
        user = pt.get<std::string>("Database.user");
        password = pt.get<std::string>("Database.password");
        host = pt.get<std::string>("Database.host");
        port = pt.get<std::string>("Database.port");
        timeout = pt.get<std::string>("Database.timeout");
        std::string connect = fmt::format("dbname={} user={} password={} host={} port={} connect_timeout={}", dbName, user, password, host, port, timeout);
        maxconnect = pt.get<int>("Handler.dbConnectAttemps");
        attempts = 1;
        waitTime = pt.get<int>("Handler.dbWaitTime");

        while(attempts <= maxconnect){
            try{
                db = new pqxx::connection(connect);
                break;
            }catch(...){
                std::cout<< "Could not Connect to database... Waiting 30's then trying again: Attempt " << attempts << " of " << maxconnect<<std::endl;
                attempts+=1;
                sleep(waitTime);

            }
        }
        if(attempts == maxconnect){
            std::cout << "Could not connect to database : limit reached" <<std::endl;
            exit(-1);
        }

        /**
        * @brief Tests database is wiped correctly
        */
        SECTION ("Wipe Database")
        {
            pqxx::work w(*db);

            std::string drop = "DROP TABLE IF EXISTS Stages, Users, StagesCompleted;";
            w.exec(drop);
            

            std::string sql = "CREATE TABLE Stages (StageID SERIAL PRIMARY KEY NOT NULL,StageName VARCHAR(255) NOT NULL,Description VARCHAR(255) NOT NULL,Clue VARCHAR(255) NOT NULL,Solution VARCHAR(255) NOT NULL,Longitude float(8) NOT NULL,Latitude float(8) NOT NULL,Cluster VARCHAR(255));";
            w.exec(sql);

            sql = "CREATE TABLE Users(UserID UUID NOT NULL DEFAULT uuid_generate_v1(),UserName VARCHAR(16) NOT NULL,Email VARCHAR(255) NOT NULL,PasswordHash TEXT NOT NULL,AdminFlag BOOLEAN NOT NULL,CurrentStage int NOT NULL REFERENCES Stages(StageID),PRIMARY KEY (UserID));";
            w.exec(sql);

            sql = "CREATE TABLE StagesCompleted(UserID UUID NOT NULL REFERENCES Users(UserID),StageID int NOT NULL REFERENCES Stages(StageID),PRIMARY KEY (UserID, StageID));";
            w.exec(sql);
            w.commit();

            messageHandler testHandler(TESTCONF);

            /**
            * @brief Tests adding new stage to database
            */
            SECTION ( "Adds new stage to database successfully" )
            {
                const std::string& data = "AddStage|b|c|d|e|1.234|0.123|h";
                testHandler.handleAddStage(data);
                pqxx::work w(*db);
                std::string sql = "SELECT * FROM Stages;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                CHECK(row[1].as<std::string>() == "b");
                CHECK(row[2].as<std::string>() == "c");
                CHECK(row[3].as<std::string>() == "d");
                CHECK(row[4].as<std::string>() == "e");
                CHECK(row[5].as<std::string>() == "1.234");
                CHECK(row[6].as<std::string>() == "0.123");
                CHECK(row[7].as<std::string>() == "h");

            }


            /**
            * @brief Tests updating stage in database
            */
            SECTION ( "Updates stage in database successfully")
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h"; 
                testHandler.handleAddStage(data);

                data = "UpdateStage|1|description|clue descript"; 
                testHandler.handleUpdateStage(data);

                std::string sql = "SELECT * FROM Stages;";
                pqxx::work w(*db);
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];

                REQUIRE(row[2].as<std::string>() == "clue descript");
            }


            /**
            * @brief Tests getting stage from database
            */
            SECTION ( "Gets stage from db" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h"; 
                testHandler.handleAddStage(data);
                data = "GetStage|1";
                std::string outData = "1,b,c,d,e,1.234,0.123,h";
                REQUIRE (testHandler.handleGetStage(data) == outData);
            }


            /**
            * @brief Tests getting stage from database using its name
            */
            SECTION ( "Gets StageID from db using StageName" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h"; 
                testHandler.handleAddStage(data);
                data = "GetStage|b";
                REQUIRE(testHandler.handleGetStageIDFromName(data) == "1");
            }


            /**
            * @brief Tests adding new user to database
            */
            SECTION ("Adds a new user to the database")
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h";
                testHandler.handleAddStage(data);
                data = "AddUser|name|place@email|helai929xjsl|0";
                testHandler.handleAddUser(data);

                std::string sql = "SELECT * FROM Users;";
                pqxx::work w(*db);
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];

                CHECK(row[1].as<std::string>() == "name");
                CHECK(row[2].as<std::string>() == "place@email");
                CHECK(row[3].as<std::string>() == "helai929xjsl");
                CHECK(row[4].as<std::string>() == "f");
                CHECK(row[5].as<std::string>() == "1");
            }


            /**
            * @brief Tests getting a user's current stage from the database
            */
            SECTION ( "Gets users current stage from db" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h";
                testHandler.handleAddStage(data);
                data = "AddUser|name|place@email|helai929xjsl|0";
                testHandler.handleAddUser(data);
                pqxx::work w(*db);
                std::string sql = "SELECT UserID FROM Users;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                std::string UserID = row[0].as<std::string>();
                data = fmt::format("GetCurrentUserStage|{}", UserID);
                REQUIRE( testHandler.handleGetCurrentUserStage(data) == "1" );

            }


            /**
            * @brief Tests getting user data using UserID
            */
            SECTION ( "Gets user from UserID" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h"; 
                testHandler.handleAddStage(data);
                data = "AddUser|name|place@email|helai929xjsl|0";
                testHandler.handleAddUser(data);
                pqxx::work w(*db);
                std::string sql = "SELECT UserID FROM Users;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                std::string UserID = row[0].as<std::string>();
                data = fmt::format("GetUserFromUserID|{}", UserID);
                std::string outData = fmt::format("{},name,place@email,helai929xjsl,f,1", UserID);
                REQUIRE(testHandler.handleGetUserFromUserID(data) == outData);

            }


            /**
            * @brief Tests getting user data using UserName
            */
            SECTION ( "Gets user from UserName" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h"; 
                testHandler.handleAddStage(data);
                data = "AddUser|name|place@email|helai929xjsl|0";
                testHandler.handleAddUser(data);
                pqxx::work w(*db);
                std::string sql = "SELECT UserID FROM Users;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                std::string UserID = row[0].as<std::string>();
                data = "GetUserFromUserName|name";
                std::string outData = fmt::format("{},name,place@email,helai929xjsl,f,1", UserID);
                REQUIRE(testHandler.handleGetUserFromUserName(data) == outData);
            }


            /**
            * @brief Tests updating user in database
            */
            SECTION ( "Updates user info successfully" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h";
                testHandler.handleAddStage(data);
                data = "AddUser|name|place@email|helai929xjsl|0";
                testHandler.handleAddUser(data);
                pqxx::work w(*db);
                std::string sql = "SELECT UserID FROM Users;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                std::string UserID = row[0].as<std::string>();

                data = fmt::format("UpdateUser|{}|email|newemail@gmail.com", UserID);
                testHandler.handleUpdateUser(data);

                sql = "SELECT * FROM Users;";
                rst = w.exec(sql);
                pqxx::row const row2 = rst[0];

                REQUIRE(row2[2].as<std::string>() == "newemail@gmail.com");

            }


            /**
            * @brief Tests getting list of all users from database
            */
            SECTION ( "Gets list of users from db" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h";
                testHandler.handleAddStage(data);
                data = "AddUser|name|place@email|helai929xjsl|0";
                testHandler.handleAddUser(data);
                pqxx::work w(*db);
                std::string sql = "SELECT UserID FROM Users;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                std::string UserID = row[0].as<std::string>();
                data = fmt::format("{},name,place@email,helai929xjsl,f,1|", UserID);              
                REQUIRE(testHandler.handleGetUserList() == data);

            }


            /**
            * @brief Tests removal of a user from the database
            */
            SECTION ( "Removes user from db" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h";
                testHandler.handleAddStage(data);
                data = "AddUser|name|place@email|helai929xjsl|0";
                testHandler.handleAddUser(data);

                pqxx::work w(*db);
                std::string sql = "SELECT UserID FROM Users;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                std::string UserID = row[0].as<std::string>();
                
                data = fmt::format("RemoveUser|{}", UserID);
                testHandler.handleRemoveUser(data);

                sql = "SELECT CASE WHEN EXISTS (SELECT * FROM Users LIMIT 1) THEN 1 ELSE 0 END;";
                rst = w.exec(sql);
                pqxx::row const row2 = rst[0];
                
                CHECK(row2[0].as<std::string>() == "0");

            }


            SECTION ( "Removes stage from db" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h";
                testHandler.handleAddStage(data);
                pqxx::work w(*db);
                std::string sql = "SELECT StageID FROM Stages;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                int stageID = row[0].as<int>();
                
                data = fmt::format("RemoveStage|{}", stageID);
                testHandler.handleRemoveStage(data);

                sql = "SELECT CASE WHEN EXISTS (SELECT * FROM Stages LIMIT 1) THEN 1 ELSE 0 END;";
                rst = w.exec(sql);
                pqxx::row const row2 = rst[0];
                
                CHECK(row2[0].as<std::string>() == "0");

            }           


            /**
            * @brief Tests adding new completed stage for user to database
            */
            SECTION ( "Adds new stages complete to database successfully" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h";
                testHandler.handleAddStage(data);
                data = "AddUser|name|place@email|helai929xjsl|0";
                testHandler.handleAddUser(data);
                pqxx::work w(*db);
                std::string sql = "SELECT UserID FROM Users;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                std::string UserID = row[0].as<std::string>();

                data = fmt::format("AddStageComplete|{}|1", UserID);

                testHandler.handleAddStageComplete(data);

                sql = "SELECT * FROM StagesCompleted;";
                rst = w.exec(sql);
                pqxx::row const row2 = rst[0];

                CHECK(row2[0].as<std::string>() == UserID);
                CHECK(row2[1].as<std::string>() == "1");

            }


            /**
            * @brief Tests getting list of user's completed stage from database
            */
            SECTION ( "Gets list of current user stages from db" )
            {
                std::string data = "AddStage|b|c|d|e|1.234|0.123|h";
                testHandler.handleAddStage(data);
                data = "AddUser|name|place@email|helai929xjsl|0";
                testHandler.handleAddUser(data);
                pqxx::work w(*db);
                std::string sql = "SELECT UserID FROM Users;";
                pqxx::result rst = w.exec(sql);
                pqxx::row const row = rst[0];
                std::string UserID = row[0].as<std::string>();

                data = fmt::format("AddStageComplete|{}|1", UserID);
                testHandler.handleAddStageComplete(data);

                sql = fmt::format("SELECT StageID FROM StagesCompleted WHERE UserID = '{}'", UserID);
                rst = w.exec(sql);
                pqxx::row const StageID = rst[0];
                std::string outData = fmt::format("{}|", StageID[0].as<std::string>());
                data = fmt::format("GetUserCompleteStages|{}", UserID);

                REQUIRE(testHandler.handleGetUserCompleteStages(data) == outData);

            }

            


        }
    }
}
