<?php
    /*
    BEWARE this file is NOT generating HTML so you cannot
    use <!-- comments etc... Keep it all PHP

    TODO:
    - User needs to be set in index.php
    - Database needs to be modified when stage is found
    */
    $stageName = array("Rice", "Salt", "Chilli", "Soy Sauce", "Sake", "Egg", "Dimsum", "Chopstick", "Chicken");
    $user_uuid = $_GET['user'];
    $task = $_GET['task'];
    // SESSION GUARD FOR user_uuid

    if ($task == "update") {
        // Update clues to be displayed on map
        queryClues($user_uuid);
    } else if ($task == "stagefound") {
        $stageid = $_GET['stageid'];
        // TODO Move user to next stage in database (this will cause queryClues() to remove the clue on the map)
        // Also tell user he's unlocked a new ingredient, or whatever?
        // It's celebration time here  -> dbinterface: addStageComplete

        // Send response as updated clues
        // to be displayed on map
        queryClues($user_uuid);
    } else {
        echo("Bad Request!\n");
    }

    function queryClues($user_uuid) {
        // Use database query to get stages for user 'user_uuid' in
        // format:
        // StageID|Lng|Lat

        // TODO get data from:
        /*
       Get Stages in cluster that are not compluser_uuiduser_uuideted 0_o
       Worried about this :(
         */
        $stageName = array("Rice", "Salt", "Chilli", "Soy Sauce", "Sake", "Egg", "Dimsum", "Chopsticks", "Chicken");
        $queue = new ZMQSocket(new ZMQContext(), ZMQ::SOCKET_REQ);
        $queue->connect("tcp://controller:5555");

        $queue->send("GetCurrentUserStage|".$user_uuid);
        $currStage = $queue->recv();
        for ($i = $currStage; $i < 9; $i++){
            $queue->send("GetStageIDFromName|".$stageName[$i]);
            $stageID = $queue->recv();

            $queue->send("GetStage|".$stageID);
            $stageArr = explode(',', $queue->recv());
            echo($stageID."|".$stageArr[5]."|".$stageArr[6]."\n");
        }
    }

?>