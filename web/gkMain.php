<php

?>
<html lang="en">
    <head>
        <title>Main page title</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel='stylesheet' href="css/stylesheetMain.css" type='text/css'>
        <link rel="stylesheet" href="https://use.typekit.net/xof2zdw.css">
        <link href="css/simple-sidebar.css" rel="stylesheet">
        <link rel="icon" href="img/icon.png">
        <script src="js/gkJs.js" type="text/javascript"></script></script>
    </head>
    <?php if(isset($_POST['gkShowPanel'])) {
        echo("<body id='gk-body' onload='changePanel(".$_POST['gkShowPanel'].")'>");
    } else {
        echo("<body id='gk-body'>");}
    ?>
    <?php if ($_POST['unameInput'] == NULL) {echo("<script type='text/javascript'>window.location.replace('login.php');</script>");}?>
        <nav class="navbar navbar-expand-lg navbar-light">
                <img class="navbarLogo" src="img/logos/dayo-white-horizontal.svg">
                <button class="navbar-toggler navbar-dark gkToggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            
                <div class="collapse navbar-collapse gkNav" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item gkNav">
                            <button class="nav-link" style="border: none; background: none;" data-toggle="collapse" onclick="changePanel(dashboardCont)">Home</button>
                        </li>
                        <li class="nav-item gkNav">
                            <button class="nav-link" style="border: none; background: none;" data-toggle="collapse" onclick="changePanel(profileCont)">Profile</button>
                        </li>
                        <li class="nav-item gkNav">
                            <button class="nav-link" style="border: none; background: none;" data-toggle="collapse" onclick="changePanel(cluesCont)">Active Clues</button>
                        </li>
                        <li class="nav-item gkNav">
                            <button class="nav-link" style="border: none; background: none;" data-toggle="collapse" onclick="changePanel(keepersCont)">Player Accounts</button>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0 btnNav">
                        <a class="btn btn-outline-success my-2 my-sm-0" href="login.php">Sign Out</a>
                    </form>
                </div>
                <form class="form-inline my-2 my-lg-0 btnMain">
                        <a class="btn btn-outline-success my-2 my-sm-0" href="adminLogin.php">Sign Out</a>
                </form>
            </nav>
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <div class="bg-light border-right" id="sidebar-wrapper">
                <div class="sidebar-heading"><img src="img/egg.svg" style="width:40px; height:40px;border-radius: 50%; margin-right: 20px;">
                @<?php echo($_POST['unameInput']);?></div>
                <div class="list-group list-group-flush">
                    <button class="list-group-item list-group-item-action bg-light" onclick="changePanel(dashboardCont)">Home</button>
                    <button class="list-group-item list-group-item-action bg-light" onclick="changePanel(profileCont)">Profile</button>
                    <button class="list-group-item list-group-item-action bg-light" onclick="changePanel(cluesCont)">Active Clues</button>
                    <button class="list-group-item list-group-item-action bg-light" onclick="changePanel(keepersCont)">Player Accounts</button>
                </div>
            </div>
                <!-- /#sidebar-wrapper -->
            
                <!-- Page Content -->
            <div id="page-content-wrapper">
            
            
                <div class="container-fluid gkConts" id="dashboardCont"> <!-- Main page container -->
                    <h1 class="mt-4">Game Management</h1>
                    <p>Welcome to the game manager!<br><br>
                        Use the side menu to switch between panels. You can go to:<br>
                        <ul>
                            <li>Profile: View and manage your own administrator account.</li>
                            <li>Active Clues: Add, update or delete clues from the current game.</li>
                            <li>Player Accounts: View and manage active player accounts.</li>
                        </ul>
                        For any questions or problems about the game management panel, or the game itself,
                        please contact the developers.
                    </p>
                </div>
                <script type="text/javascript">
                    var x = document.getElementById("dashboardCont");
                    x.style.display = "block";
                </script>
                <div class="container-fluid gkConts" id="profileCont"> <!-- Main page container -->
                    <h1 class="mt-4">Profile</h1>
                    <div class="card card-body text-center">
                        <img src="img/egg.svg" style="width:75px; height:75px;border-radius: 50%; margin: 20px auto;">
                        <p>@<?php echo($_POST['unameInput']);?>'s account:<br><br>Created: Feb 2020<br>Birthday: 29/07/2000<?php if(isset($_POST['emailInput'])){ echo("<br>Email: ".$_POST['emailInput']); }?></p>
                        <a class="btn btn-primary" href="editProfile.php">Edit Profile</a>
                    </div>
                </div>
                <div class="container-fluid gkConts" id="cluesCont"> <!-- Main page container -->
                    <h1 class="mt-4">Active Clues</h1>
                    <p>Welcome to the game manager!</p>
                    <p>Make changes to each clue and (some other shit here)</p>
                    <?php
                        if (isset($_POST['errorMsg'])) {
                            echo("<p class='error'>* ".$_POST['errorMsg']."</p>");
                        }
                        /*$active = 9;
                        $stageName = array("Rice", "Salt", "Chilli", "Soy Sauce", "Sake", "Egg", "Dimsum", "Chopsticks", "Chicken");
                        $descriptions = array("this is a description", "also a description", "another description", "yet another", "there's still more", "they keep on coming", "how many left?", "there's one more", "thank god");
                        $clues = array("this is a clue",
                                        "also a clue",
                                        "another clue",
                                        "yet another",
                                        "there's still more",
                                        "they keep on coming",
                                        "how many left?",
                                        "there's one more",
                                        "thank god");
                        $solutions = array("2012", "2012", "2012", "2012", "2012", "2012", "2012", "2012", "2012");
                        $longitudes = array(50.735515, 50.735515, 50.735515, 50.735515, 50.735515, 50.735515, 50.735515, 50.735515, 50.735515);
                        $latitudes = array(-3.533937, -3.533937, -3.533937, -3.533937, -3.533937, -3.533937, -3.533937, -3.533937, -3.533937);
                        $clusters = array("Forum", "Forum", "Forum", "Forum", "Forum", "Forum", "Forum", "Forum", "Forum");*/

                        $allPrinted = FALSE;
                        $active = 0;

                    
                        echo("<form class='gk-clues-form w-100' action='utils/updateClues.php' method='POST'>
                              <input type='submit' class='btn btn-primary mb-3' value='Save Changes'>
                              <input type='number' class='d-none' name='noClues' value='".$active."'>
                              <a class='btn btn-primary mb-3' style='color:white;'>Reset</a>");
                        while ($allPrinted == FALSE && $active < 9) {
                            $stageIDreq = "GetStageIDFromName|".$stageName[$active];
                            $queue->send($stageIDreq);
                            $stageID = $queue->recv();

                            if($stageID == "") {
                                $allPrinted = TRUE;
                            } else {
                                $queue->send("GetStage|".$stageID);
                                $stageData = explode(",", $queue->recv());

                                echo("<div class='gk-clue-div shadow-sm'>
                                        <table class='w-100 ml-auto mr-auto'>
                                            <tbody>
                                                <tr>
                                                    <td class='clues-data mb-2'>
                                                        <p class='mb-2'>
                                                            <b>".$stageData[0]."</b> -
                                                            <button type='button' class='border-0 bg-transparent m-0 p-0' onclick='deleteClue(".$active.")'>
                                                                <label class='form-text text-muted m-0'>delete clue</label>
                                                            </button>
                                                        </p>
                                                    </td></tr>
                                            </tbody>
                                        </table>
                                        <label class='form-text text-muted'>Longitude/latitude coordinates:</label>
                                        <table class='w-100'>
                                            <tbody>
                                                <tr>
                                                    <td class='clues-data w-50'><input class='w-100' type='number' name='long-".$active."' value='".$stageData[5]."' placeholder='Longitude'></td>
                                                    <td class='clues-data w-50'><input class='w-100' type='number' name='latit-".$active."' value='".$stageData[6]."' placeholder='Latitude'></td></tr>
                                            </tbody>
                                        </table>
                                        <table class='w-100'>
                                            <tbody>
                                                <tr><td class='clues-data'><input class='w-100' type='textarea' name='clue-".$active."' value='".$stageData[3]."' placeholder='Enter Clue'></td></tr>
                                                <tr><td class='clues-data'><input class='w-100' type='textarea' name='sol-".$active."' value='".$stageData[4]."' placeholder='Enter Solution'></td></tr>
                                                <tr><td class='clues-data'><label class='form-text text-muted'>Description:</label></td></tr>
                                                <tr><td class='clues-data'><input class='w-100' type='textarea' name='descr-".$active."' value='".$stageData[2]."' placeholder='Enter Description'></td></tr>
                                                <tr><td class='clues-data'><label class='form-text text-muted'>Cluster ID:</label></td></tr>
                                                <tr><td class='clues-data'><input type='text' name='clust-".$active."' value='".$stageData[7]."' placeholder='Enter Cluster ID'></td></tr>
                                            </tbody>
                                        </table>
                                    </div>");
                                $active += 1;
                            }
                        }
                        if ($active == 0) {
                            echo("<div class='gk-clue-div shadow-sm text-center'>
                                    <p>No currently active clues.</p>
                                </div>");
                        }
                        echo("<input type='number' class='d-none' name='noClues' value='".$active."'>
                                </form>
                                <form class='d-none' id='delete-clue-form' action='utils/deleteClue.php' method='POST'>
                                    <input type='text' name='stageNameDel' id='stageNameDel' value='stage'>
                                    <input type='number' name='stageIndexDel' id='stageIndexDel' value='0'>
                                    <input type='number' name='noClues' id='noClues' value='".$active."'>
                                </form>");
                        if ($active < 9) {
                            echo("<div class='add-clue-btn'>
                                    <a href='index'><span class='link-div'></span></a>
                                    <img src='img/plus.svg' class='add-clue-plus'>
                                </div>");
                        } else {
                            echo("<div class='add-clue-btn-dis'>
                                    <img src='img/plus.svg' class='add-clue-plus'>
                                </div>");
                        }
                        
                    ?>
                    <script type='text/javascript'>
                        var ingredients = ['Rice', 'Salt', 'Chilli', 'Soy Sauce', 'Sake', 'Egg', 'Dimsum', 'Chopsticks', 'Chicken'];
                        function deleteClue(index) {
                            if(confirm("Are you sure you want to delete this clue?")){
                                document.getElementById('stageNameDel').value = ingredients[index];
                                document.getElementById('stageIndexDel').value = index;
                                document.getElementById('delete-clue-form').submit();
                            }
                        }
                    </script>
                </div>
                <div class="container-fluid gkConts" id="keepersCont"> <!-- Main page container -->
                    <h1 class="mt-4">Keepers</h1>
                    <p>View all active game keeper accounts for the current game.</p>
                    
                    <?php

                        $accounts = 14;
                        echo("<table class='w-100'><tbody>");

                        for ($i = 1; $i <= $accounts; $i++){
                            if (($i - 1) % 3 == 0) {
                                echo("<tr>");
                            }
                            echo("<td profile-cards><div class='card card-body text-center shadow'>
                                    <img class='profile-pic' src='img/egg.svg'>
                                    <p>@account".$i."</p>
                                    <form action='utils/deleteAccount.php' class='w-100'>
                                        <input class='d-none' type='text' name='delAccount' value='account".$i."'>
                                        <input class='d-none' type='text' name='deleter' value='gk'>
                                        <input class='btn btn-primary' type='submit' value='Delete User'>
                                    </form>
                                </div></td>");
                            
                            if ($i % 3 == 0) {
                                echo("</tr>");
                            }
                        }
                    ?>
                </div>
                <div class="container-fluid gkConts" id="eventsCont"> <!-- Main page container -->
                    <h1 class="mt-4">Story Editor</h1>
                    <p>Create conversations at different points in the game, triggered by finding an ingredient</p>
                </div>
                <div class="container-fluid gkConts" id="statusCont"> <!-- Main page container -->
                    <h1 class="mt-4">Page Editor</h1>
                    <p>Edit files for rules window/FAQs?</p>
                </div>
            </div>
                <!-- /#page-content-wrapper -->
            
        </div>
          <!-- /#wrapper -->
        

        
          <!-- Menu Toggle Script -->
          <script>
            $("#menu-toggle").click(function(e) {
              e.preventDefault();
              $("#wrapper").toggleClass("toggled");
            });
          </script>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

</html>
