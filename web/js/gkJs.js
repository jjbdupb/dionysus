
function changePanel(panel) {
    hideAllPanels();
    panel.style.display = "block";
}

function hideAllPanels() {
    document.getElementById("dashboardCont").style.display = "none";
    document.getElementById("profileCont").style.display = "none";
    document.getElementById("cluesCont").style.display = "none";
    document.getElementById("keepersCont").style.display = "none";
}

//Export functions to be tested
module.exports = {
    changePanel
}
