//To setup Jest run npm i -D jest
//To start test run npm test

const listeners = window._virtualConsole.listeners("jsdomError");
const originalListener = listeners && listeners[0];
window._virtualConsole.removeAllListeners("jsdomError");
window._virtualConsole.addListener("jsdomError", (error) => {
  if (error.type !== "not implemented" && originalListener) {
    originalListener(error);
  }
  // swallow
});

//Import functions
const functions = require('./jss');

const startConvoContent = `<div class='speech-overlay overlay' id='convo".$_POST['convoNo']."'>
<img class='character character-left' src='img/characters/tanaka-plain.svg' id='character".$_POST['convoNo']."'>
<img class='character character-right' src='img/characters/' id='character2'>
<div class='speech-bubble' id='bubble".$_POST['convoNo']."' data-parent='#bubbles'>
    <p class='convo-text first-convo' id='bubble".$_POST['convoNo']."Txt'></p>
    <button class='convo-continue first-convo-btn shadow' id='bubble".$_POST['convoNo']."Btn' onclick='continueConvo(bubble".$_POST['convoNo']."Txt, ".$_POST['convoNo'].", convo".$_POST['convoNo'].", character".$_POST['convoNo'].", bubble".$_POST['convoNo'].")'>Next</button>
</div>
</div>

`;

const makeGuessContent = `<div class='infoContainer card speech-style' id='gInput'>
<div class='form-group'>
    <p class='guess-input-clue'>".$clue."</p>
    <input class='guess-input-text' type='text' id='guessBox'>
    <button class='first-convo-btn shadow' onclick='makeGuess(guessBox,clueAns)'>Submit Guess</button>
</div>
<div class='d-none'>
    <form action='index.php' method='post' id='clueForm'>
        <input type='text' name='unameInput' value='".$_POST['unameInput']."' id='unameInput'>
        <input type='text' name='emailInput' value='".$_POST['emailInput']."' id='emailInput'>
        <input type='number' id='clueNo' name='clueNo' value='".$itemNo."'>
        <p id='clueAns'>".$ans."</p>
    </form>
</div>
</div>`;

const submitGuessContent = `<td><div class='card card-body clue-card clue-found'>
<a href='#' id='guessLink'><span class='link-div'></span></a>
<form class='d-none' id='makeGuessForm'>
    <input type='text' name='unameInput' value='".$_POST['unameInput']."' id='unameInput'>
    <input type='text' name='emailInput' value='".$_POST['emailInput']."' id='emailInput'>
    <input type='number' name='itemNo' value='".$i."'>
    <input type='text' name='stageID' value='".$stageID."'>
</form>
<img class='circle' src='img/circle.svg'>
<img class='clue-badge badge-found' src='img/ingredients/clue".$i."-hidden.svg' id='clue".$i."'>
<p>Next Clue</p></div></td>`;

//startConvo test cases
test("Test startConvo 1: Bubble text to show correct conversation line", () =>{

    document.body.innerHTML = startConvoContent;

    const convoId = document.getElementsByClassName("speech-overlay overlay")[0];
    const textId = document.getElementsByClassName("convo-text first-convo")[0];
    const convo = 5;
    const charId = document.getElementsByClassName("character character-left")[0];
    functions.startConvo(textId,convo,convoId,charId);
    expect(textId.innerHTML).toBe("Hold on...");
})


test("Test startConvo 2: Convo style display to be block", () =>{

    document.body.innerHTML = startConvoContent;

    const convoId = document.getElementsByClassName("speech-overlay overlay")[0];
    const textId = document.getElementsByClassName("convo-text first-convo")[0];
    const convo = 5;
    const charId = document.getElementsByClassName("character character-left")[0];
    functions.startConvo(textId,convo,convoId,charId);
    expect(convoId.style.display).toBe("block");
})

test("Test startConvo 3: Correct character image to be shown", () =>{

    document.body.innerHTML = startConvoContent;

    const convoId = document.getElementsByClassName("speech-overlay overlay")[0];
    const textId = document.getElementsByClassName("convo-text first-convo")[0];
    const convo = 5;
    const charId = document.getElementsByClassName("character character-left")[0];
    functions.startConvo(textId,convo,convoId,charId);
    expect(charId.src).toBe("http://localhost/img/characters/character-8-talking.svg");
})

//makeGuess test cases
test("Test makeGuess 1: Guess box will show wrong answer if wrong answer is given", () =>{

    document.body.innerHTML = makeGuessContent;

    var txtBoxId = document.getElementById("guessBox");
    var correctAns = document.getElementById("clueAns");
    txtBoxId.value = "Wrong";
    correctAns.innerHTML = "Correct";
    functions.makeGuess(txtBoxId, correctAns);
    expect(txtBoxId.classList).toContain("wrong-answer");
})

test("Test makeGuess 2: Form will be submitted if correct answer is given", () =>{

    document.body.innerHTML = makeGuessContent;

    var check = false;
    function checkSubmit(){
        check = true;
    }
    var txtBoxId = document.getElementById("guessBox");
    var correctAns = document.getElementById("clueAns");
    var clueForm = document.getElementById("clueForm");
    clueForm.onsubmit = function() {checkSubmit()};
    txtBoxId.value = "correct";
    correctAns.innerHTML = "Correct";
    functions.makeGuess(txtBoxId, correctAns);
    expect(check).toBeTruthy();
})


//continueConvo test cases
test("Test continueConvo 1: Bubble text to not contain first convo", () =>{

    document.body.innerHTML = startConvoContent;

    const convoId = document.getElementsByClassName("speech-overlay overlay")[0];
    const textId = document.getElementsByClassName("convo-text first-convo")[0];
    const convo = 5;
    const charId = document.getElementsByClassName("character character-left")[0];
    const bubbleId = document.getElementsByClassName("speech-bubble")[0];
    functions.continueConvo(textId,convo,convoId,charId,bubbleId);
    expect(textId.classList.contains("first-convo")).toBeFalsy();
})

test("Test continueConvo 2: Bubble text to contain next convo", () =>{

    document.body.innerHTML = startConvoContent;

    const convoId = document.getElementsByClassName("speech-overlay overlay")[0];
    const textId = document.getElementsByClassName("convo-text first-convo")[0];
    const convo = 5;
    const charId = document.getElementsByClassName("character character-left")[0];
    const bubbleId = document.getElementsByClassName("speech-bubble")[0];
    functions.continueConvo(textId,convo,convoId,charId,bubbleId);
    expect(textId.classList.contains("next-convo")).toBeTruthy();
})

//submitGuess test case
test("Test submitGuess 1: Guess form to be submitted", () =>{

    document.body.innerHTML = submitGuessContent;

    var check = false;
    function checkSubmit(){
        check = true;
    }
    var clueForm = document.getElementById("makeGuessForm");
    clueForm.onsubmit = function() {checkSubmit()};
    functions.submitGuess();
    expect(check).toBeTruthy();
})




