// Constants
const CLUE_HALF_EXTENTS = [.0001, .0001];

// these are for mock pos
const TICK_DELTA = 20.0;
const LOCATION_FORUM = [50.735474, -3.533201];

// Constructor (dont call directly use initializeMap)
function Map(leafletMap, user) {
    this.leafletMap = leafletMap;
    this.user = user;

    this.clueIcon = L.icon({
        iconUrl: "img/clue.png",
        shadowUrl: "img/clue_shadow.png",
        iconSize: [64, 64],
        shadowSize: [64, 64],
        iconAnchor: [16, 60],
        shadowAnchor: [16, 60],
        popupAnchor: [0, 0]
    });

    this.marker = L.marker(LOCATION_FORUM);
    this.marker.addTo(this.leafletMap);

    this.currentStages = null;
    this.stagesFound = [];

    this.update();
}

// live update geoloc is tracked pos
Map.prototype.playermove = function(geoLoc) {
    this.marker.setLatLng(geoLoc);

    if(this.currentStages) {
        for (var i = 0; i < this.currentStages.length; i++) {
            var stage = this.currentStages[i];
            var minLat = stage.loc.lat - CLUE_HALF_EXTENTS[0] + 0.0001;
            var maxLat = stage.loc.lat + CLUE_HALF_EXTENTS[0] + 0.0001;
            var minLng = stage.loc.lng - CLUE_HALF_EXTENTS[1];
            var maxLng = stage.loc.lng + CLUE_HALF_EXTENTS[1];

            // Player intersects clue bounds
            if ((geoLoc.lat > minLat && geoLoc.lat < maxLat) &&
                    (geoLoc.lng > minLng && geoLoc.lng < maxLng)) {
                // Not already found (otherwise we would send update requests)
                // all the time when overlapping the clue
                if (this.stagesFound.indexOf(stage.stageid) <= -1) {
                    this.update(stage.stageid);
                    this.stagesFound.push(stage.stageid);

                    // ALERT IS HERE IF IT NEEDS TO BE REMOVED ----------------------------------------------------------------------
                    alert("Clue found!!");
                }
            }
        }
    }

}

Map.prototype.update = function(stageIdFound) {
    var reqStr = "mapclues.php?user=" + this.user;

    if (stageIdFound === undefined || stageIdFound === null) {
        reqStr += "&task=update";
    } else {
        reqStr += "&task=stagefound&stageid=" + stageIdFound;
    }

    var mapReq = new XMLHttpRequest();
    var thisMap = this;
    mapReq.open("GET", reqStr);
    mapReq.onload = function() {
        // Remove old markers
        if (thisMap.currentStages !== null) {
            for(var i = 0; i < thisMap.currentStages.length; i++) {
                var stage = thisMap.currentStages[i];
                stage.marker.remove();
            }
        }

        thisMap.currentStages = thisMap._readStages(mapReq.responseText);

        if (thisMap.currentStages) {
            // For now just add markers (and store in stages)
            for(var i = 0; i < thisMap.currentStages.length; i++) {
                var stage = thisMap.currentStages[i];

                stage.marker = L.marker(stage.loc, {
                    icon: thisMap.clueIcon});
                stage.marker.addTo(thisMap.leafletMap);
                
                //test marker:
                //L.marker(stage.loc).addTo(thisMap.leafletMap);
            }
        }
    }
    mapReq.send();
}

// Returns array of 'stages'(clues) a 'stage' is
// a 'stageid' and a Leaflet LatLng location
Map.prototype._readStages = function(response) {
    if(response === "")
        return null;

    response = response.trim();

    var lines = response.split("\n");
    var stages = [];

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i].trim();
        var stageArgs = line.split("|");

        console.log(line);
        console.log(stageArgs);

        // 'Stage' definition
        stages.push({
            stageid: parseInt(stageArgs[0]),
            loc: {
                // DB has Lng|Lat, Leaflet expects Lat|Lng
                lat: parseFloat(stageArgs[2]),
                lng: parseFloat(stageArgs[1])
            },
            marker: null
        });
    }

    return stages;
}

// 'static' function creates map in div 'map' (must pre-exist)
Map.initializeMap = function(user) {
    if (!navigator.geolocation) {
        alert("HTML5 GeoLocation not supported!!");
        return null;
    }

    var leaflet = L.map("map").setView(LOCATION_FORUM, 22);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        tileSize: 256
    }).addTo(leaflet);

    var map = new Map(leaflet, user);

    
    // Should be replaced with setupGeoLoc() when not debugging
    //setupMockGeoLoc(map);
    
    if(!setupGeoLoc(map)) {
        return null;
    }

    return map;
}

function setupGeoLoc(map) {
    if (!navigator.geolocation) {
        alert("HTML5 gelocation not available!!");
        return false;
    }

    function success(position) {
        var geoLoc = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };

        map.playermove(geoLoc);
    }

    function error(positionErr) {
        console.log(positionErr);
    }

    navigator.geolocation.watchPosition(success, error);

    return true;
}

/*
function setupMockGeoLoc(map) {
    var mockGeoLoc = {lat: LOCATION_FORUM[0], lng: LOCATION_FORUM[1]};
    var keysDown = [];

    // WSAD - 87 83 65 68

    var mapDiv = document.getElementById("map");
    mapDiv.onkeyup = function(ev) {
        var ind = keysDown.indexOf(ev.keyCode);
        if(ind > -1) {
            keysDown.splice(ind, 1);
        }
    }
    mapDiv.onkeydown = function(ev) {
        if (!keyDown(ev.keyCode)) {
            if (ev.keyCode == 69) { // 'E' key
                console.log("lat: " + mockGeoLoc.lat + ", lng: " + mockGeoLoc.lng);
            }

            keysDown.push(ev.keyCode);
        }
    }
    mapDiv.addEventListener('focusout', function(){
        keysDown = [];
    });
    function keyDown(key) {
        return keysDown.indexOf(key) > -1;
    }

    setInterval(function() {
        const SPD = 0.00001;
        var moved = false;
        if (keyDown(87)){ mockGeoLoc.lat += SPD; moved = true; }
        if (keyDown(83)){ mockGeoLoc.lat -= SPD; moved = true; }
        if (keyDown(65)){ mockGeoLoc.lng -= SPD; moved = true; }
        if (keyDown(68)){ mockGeoLoc.lng += SPD; moved = true; }

        if(moved) {
            map.playermove(mockGeoLoc);
        }
    }, TICK_DELTA);
}*/
