//To setup Jest run npm i -D jest
//To start test run npm test

//Import functions
const functions = require('./gkJs');

const pageContent = `<div id="page-content-wrapper">
            
            
<div class="container-fluid gkConts" id="dashboardCont"> <!-- Main page container -->
    <h1 class="mt-4">Game Management</h1>
    <p>Welcome to the game manager!<br><br>
        Use the side menu to switch between panels. You can go to:<br>
        <ul>
            <li>Profile: View and manage your own administrator account.</li>
            <li>Active Clues: Add, update or delete clues from the current game.</li>
            <li>Game Keepers: View other game keeper accounts</li>
            <li>Story Editor: Create and manage the story for the player.</li>
            <li>Page Editor: Edit what certain windows show</li>
        </ul>
        For any questions or problems about the game management panel, or the game itself,
        please contact the developers.
    </p>
</div>
<script type="text/javascript">
    var x = document.getElementById("dashboardCont");
    x.style.display = "block";
</script>
<div class="container-fluid gkConts" id="profileCont"> <!-- Main page container -->
    <h1 class="mt-4">Profile</h1>
    <div class="card card-body text-center">
        <img src="img/egg.svg" style="width:75px; height:75px;border-radius: 50%; margin: 20px auto;">
        <p>@adminStebe's account:<br><br>Created: Feb 2020<br>Birthday: 29/07/2000<br>Email: cdevans98@gmail.com</p>
        <a class="btn btn-primary" href="editProfile.php">Edit Profile</a>
    </div>
</div>
<div class="container-fluid gkConts" id="cluesCont"> <!-- Main page container -->
    <h1 class="mt-4">Clues</h1>
    <p>Welcome to game manager!</p>

    <p>
        Use draggable cards to organise clues?<br>
        <a href="https://mdbootstrap.com/plugins/jquery/sortable/">Example 1</a><br>
        <a href="https://bootsnipp.com/snippets/4MpGn">Example 2</a>
    </p>
</div>
<div class="container-fluid gkConts" id="keepersCont"> <!-- Main page container -->
    <h1 class="mt-4">Keepers</h1>
    <p>View all active game keeper accounts for the current game.</p>

    <table>
        <tbody>
            <tr>
                <td>
                    <div class="card card-body text-center">
                        <img class="profile-pic" src="img/egg.svg">
                            <p>@adminStebe<br><br>Created: Feb 2020</p>
                    </div>
                </td>
                <td>
                    <div class="card card-body text-center">
                        <img class="profile-pic" src="img/egg.svg">
                            <p>@adminStebe<br><br>Created: Feb 2020</p>
                    </div>
                </td>
                <td>
                    <div class="card card-body text-center">
                        <img class="profile-pic" src="img/egg.svg">
                            <p>@adminStebe<br><br>Created: Feb 2020</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="card card-body text-center">
                        <img class="profile-pic" src="img/egg.svg">
                            <p>@adminStebe<br><br>Created: Feb 2020</p>
                    </div>
                </td>
                <td>
                    <div class="card card-body text-center">
                        <img class="profile-pic" src="img/egg.svg">
                            <p>@adminStebe<br><br>Created: Feb 2020</p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="container-fluid gkConts" id="eventsCont"> <!-- Main page container -->
    <h1 class="mt-4">Story Editor</h1>
    <p>Create conversations at different points in the game, triggered by finding an ingredient</p>
</div>
<div class="container-fluid gkConts" id="statusCont"> <!-- Main page container -->
    <h1 class="mt-4">Page Editor</h1>
    <p>Edit files for rules window/FAQs?</p>
</div>
</div>`


//changePanel test cases
test("Test changePanel 1: Show dashboard and hide all other panels", () =>{

    document.body.innerHTML = pageContent;

    const panel = document.getElementById("dashboardCont");
    const hiddenPanel1 = document.getElementById("profileCont");
    const hiddenPanel2 = document.getElementById("cluesCont");
    const hiddenPanel3 = document.getElementById("keepersCont");
    functions.changePanel(panel);
    expect(panel.style.display).toBe("block");
    expect(hiddenPanel1.style.display).toBe("none");
    expect(hiddenPanel2.style.display).toBe("none");
    expect(hiddenPanel3.style.display).toBe("none");
})

test("Test changePanel 2: Show profile and hide all other panels", () =>{

    document.body.innerHTML = pageContent;

    const panel = document.getElementById("profileCont");
    const hiddenPanel1 = document.getElementById("dashboardCont");
    const hiddenPanel2 = document.getElementById("cluesCont");
    const hiddenPanel3 = document.getElementById("keepersCont");
    functions.changePanel(panel);
    expect(panel.style.display).toBe("block");
    expect(hiddenPanel1.style.display).toBe("none");
    expect(hiddenPanel2.style.display).toBe("none");
    expect(hiddenPanel3.style.display).toBe("none");
})

test("Test changePanel 3: Show clues and hide all other panels", () =>{

    document.body.innerHTML = pageContent;

    const panel = document.getElementById("cluesCont");
    const hiddenPanel1 = document.getElementById("profileCont");
    const hiddenPanel2 = document.getElementById("dashboardCont");
    const hiddenPanel3 = document.getElementById("keepersCont");
    functions.changePanel(panel);
    expect(panel.style.display).toBe("block");
    expect(hiddenPanel1.style.display).toBe("none");
    expect(hiddenPanel2.style.display).toBe("none");
    expect(hiddenPanel3.style.display).toBe("none");
})

test("Test changePanel 4: Show keepers and hide all other panels", () =>{

    document.body.innerHTML = pageContent;

    const panel = document.getElementById("keepersCont");
    const hiddenPanel1 = document.getElementById("profileCont");
    const hiddenPanel2 = document.getElementById("cluesCont");
    const hiddenPanel3 = document.getElementById("dashboardCont");
    functions.changePanel(panel);
    expect(panel.style.display).toBe("block");
    expect(hiddenPanel1.style.display).toBe("none");
    expect(hiddenPanel2.style.display).toBe("none");
    expect(hiddenPanel3.style.display).toBe("none");
})

