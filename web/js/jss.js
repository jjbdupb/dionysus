var conversations = [
                    //first conversation
                        [["Hello there! Welcome to 'Ramen Dayo!'", "tanaka-plain"],
                        ["The largest ramen restaurant in East Devon!", "tanaka-laughing"],
                        ["You must be my new apprentice! Nice to meet you!", "tanaka-happytalk"],
                        ["Welcome to your new home...", "tanaka-happy"],
                        ["... my name is Mr Tanaka.", "tanaka-happytalk"],
                        ["Sorry for the wait, it's been very busy here.", "tanaka-talking"],
                        ["Everyone's preparing for the competition!", "tanaka-serioustalk"],
                        ["It's the biggest in the country...", "tanaka-talking"],
                        ["... and it's coming very soon!", "tanaka-serioustalk"],
                        ["But I have to savour it this time...", "tanaka-sad"],
                        ["Why? I'm too old to be a ramen chef now.", "tanaka-serioustalk"],
                        ["I should be retiring soon.", "tanaka-sad"],
                        ["I can't keep up with these new places.", "tanaka-sadder"],
                        ["Besides...", "tanaka-crying"],
                        ["I have a lot of sleep to catch up on!", "tanaka-laughing"],
                        ["Anyway, I should show you around...", "tanaka-happytalk"],
                        ["Wait... what was that?", "tanaka-confused"],
                        ["Everyone remain calm!", "tanaka-angry", "character2", "ninja-both", "R"],
                        ["Who are you?!", "tanaka-shouting", "character2", "ninja-both", "L"],
                        ["That's not important!", "tanaka-angry", "character2", "ninja-both", "R"],
                        ["What is important is that you can't compete!", "tanaka-angry", "character2", "ninja-both", "R"],
                        ["What do you mean?", "tanaka-shouting", "character2", "ninja-both", "L"],
                        ["Your time in the ramen competition is up Mr Tanaka!", "tanaka-angry", "character2", "ninja-both", "R"],
                        ["Go get his stuff!", "tanaka-angry", "character2", "ninja1", "R"],
                        ["Don't move Mr Tanaka!", "tanaka-angry", "character2", "ninja2-shouting", "R"],
                        ["Why are you doing this to me?", "tanaka-shouting", "character2", "ninja1", "L"],
                        ["A lot of people don't want you to compete...", "tanaka-shouting", "character2", "ninja1-shouting", "R"],
                        ["... I didn't want it to come to this.", "tanaka-sad", "character2", "ninja1-sorry", "R"],
                        ["I got everything, let's go.", "tanaka-sadder", "character2", "ninja2-shouting", "R"],
                        ["I'm sorry Mr Tanaka...", "tanaka-sadder", "character2", "ninja-both", "R"],
                        ["I don't know what just happened...", "tanaka-sad"],
                        ["...", "tanaka-sad"],
                        ["They've taken all my ingredients!", "tanaka-sadder"],
                        ["I don't know what to do...", "tanaka-sad"],
                        ["... What was that?", "tanaka-confused"],
                        ["You can help?", "tanaka-happy"],
                        ["That would be amazing!", "tanaka-happytalk"],
                        ["You should go looking around the university...", "tanaka-eyebrow"],
                        ["Maybe people know where the ingredients are.", "tanaka-talking"],
                        ["We'll be back in the competition in no time!", "tanaka-happytalk"],
                        ["Thank you, I'll hold the fort here.", "tanaka-talking"],
                        ["There's a lot of hungry customers to let down!", "tanaka-laughing"],
                        ["Check the ingredients tab to see your progress."],
                        ["Tap on an ingredient to try and solve the clue!"],
                        ["Once you have all nine ingredients..."],
                        ["You can enter the ramen competition!"],
                        ["There you'll be given the final clue."],
                        ["Good luck, and happy hunting!"]],
                    // second conversation
                        [["Hello again! You're back!", "tanaka-happy"],
                        ["So! How did it go?", "tanaka-inquisitive"],
                        ["Oh excellent! All 9!", "tanaka-happytalk"],
                        ["I'm very proud of you!", "tanaka-happy"],
                        ["Did anything interesting happen?", "tanaka-laughing"],
                        ["Hello darling...", "tanaka-serious", "character2", "character-9-talking", "R"],
                        ["Hey! When did you get back!", "tanaka-happytalk", "character2", "character-9", "L"],
                        ["It's been so long!", "tanaka-talking", "character2", "character-9", "L"],
                        ["I only got back recently...", "tanaka-happy", "character2", "character-9-talking", "R"],
                        ["I wanted to surprise you!", "tanaka-happy", "character2", "character-9-talking", "R"],
                        ["... and then your apprentice found me.", "tanaka-happy", "character2", "character-9-talking", "R"],
                        ["I'm so happy you're back!", "tanaka-happytalk", "character2", "character-9", "L"],
                        ["Just in time for the ramen competition!", "tanaka-laughing", "character2", "character-9", "L"],
                        ["You're right, there's a competition to win!", "tanaka-happy", "character2", "character-9-talking", "R"],
                        ["Together!", "tanaka-laughing", "character2", "character-9", "L"]],
                    // Ramen Competition
                        [["So, this is it!", "tanaka-talking"],
                        ["You've come a long way to be here!", "tanaka-happytalk", "character2", "character-9", "L"],
                        ["You'll be taking home the prize in no time!", "tanaka-happy", "character2", "character-9-talking", "R"],
                        ["You just need to answer the final clue...", "tanaka-happytalk", "character2", "character-9", "L"],
                        ["The rules say you have to do it alone...", "tanaka-serioustalk"],
                        ["... but we believe in you!", "tanaka-happytalk", "character2", "judge", "L"],
                        ["Oh, here's the judge, good luck!", "tanaka-serioustalk", "character2", "judge", "L"],
                        ["Hello there.", "empty", "character2", "judge-talking", "R"],
                        ["It looks like it's your turn.", "empty", "character2", "judge-mental", "R"],
                        ["Come over to the kitchen when you're ready.", "empty", "character2", "judge-talking", "R"],
                        ["... and you better be ready...", "empty", "character2", "judge-mental", "R"]],
                    // Won the competition
                        [["Well done!", "tanaka-happytalk"],
                        ["You won"]],
                    // 9th clue conversation
                        [["Hello there!", "empty", "character2", "character-9", "R"],
                        ["I've been hearing a lot about you...", "empty", "character2", "character-9-talking", "R"],
                        ["You've done well so far...", "empty", "character2", "character-9-talking", "R"],
                        ["I'm sure it hasn't been easy.", "empty", "character2", "character-9", "R"],
                        ["Let me introduce myself...", "empty", "character2", "character-9-talking", "R"],
                        ["I'm Mrs Tanaka. I think you know my husband.", "empty", "character2", "character-9-talking", "R"],
                        ["I haven't seen him for a long time.", "empty", "character2", "character-9", "R"],
                        ["... I wanted to surprise him...", "empty", "character2", "character-9", "R"],
                        ["... but I didn't know how to get his attention.", "empty", "character2", "character-9", "R"],
                        ["No! I'm not behind this all!", "empty", "character2", "character-9-talking", "R"],
                        ["But I did find the last ingredient first.", "empty", "character2", "character-9", "R"],
                        ["We should go see Mr Tanaka soon...", "empty", "character2", "character-9-talking", "R"],
                        ["Although... I'm a fan of showmanship...", "empty", "character2", "character-9", "R"],
                        ["So I'll give you the clue I had to solve!", "empty", "character2", "character-9-talking", "R"]],
                    // 8th clue conversation
                        [["Hold on...", "character-8"],
                        ["...", "character-8"],
                        ["Sorry, coursework is due soon...", "character-8-talking"],
                        ["I must solve the P = NP problem by tomorrow.", "character-8-talking"],
                        ["...", "character-8"],
                        ["Sorry, I'm beginning to think it's impossible.", "character-8-talking"],
                        ["Anyway, I can't give it to you.", "character-8-talking"],
                        ["My big presentation is on that day...", "character-8-talking"],
                        ["My internship at Noogle™ depends on it.", "character-8-talking"],
                        ["I created an AI to recognise faces.", "character-8"],
                        ["... and I got it to work 30% of the time!", "character-8"],
                        ["So, it's a pretty big deal...", "character-8-talking"],
                        ["I'd move the presentation sooner...", "character-8-talking"],
                        ["... but I can't fix all these bugs.", "character-8"],
                        ["You could help me?", "character-8-talking"],
                        ["That'd be great! Thank you!", "character-8-talking"],
                        ["Here's your clue for the ingredient!", "character-8"]],
                    // 7th clue conversation
                        [["Welcome to Prêt, how can I help you?", "character-7-talking"],
                        ["Oh, you're with Mr Tanaka...", "character-7"],
                        ["You can't have the ingredient...", "character-7"],
                        ["No students eat here during the competition.", "character-7-talking"],
                        ["Our food is expensive so we need customers!", "character-7-talking"],
                        ["Only Exeter students can afford it!", "character-7"],
                        ["We rely on these people...", "character-7-talking"],
                        ["Tell you what...", "character-7"],
                        ["If we can advertise at the competition...", "character-7-talking"],
                        ["... no Exeter student can resist our sandwiches.", "character-7"],
                        ["It's a deal.", "character-7-talking"]],
                    // 6th clue conversation
                        [["Okay everyone! Hold on a second!", "character-6-talking"],
                        ["Sorry, let's make this quick...", "character-6"],
                        ["I'm part way through my lecture.", "character-6-talking"],
                        ["I think it's pretty obvious why I helped.", "character-6-talking"],
                        ["Lecture attendance halves during the competition...", "character-6"],
                        ["... I'll only have 3 students then.", "character-6-talking"],
                        ["Studies are important! More so than ramen!", "character-6"],
                        ["Listen though...", "character-6"],
                        ["Talk to the organisers about scheduling.", "character-6-talking"],
                        ["Only then will I give your ingredient back.", "character-6-talking"]],
                    // 5th clue conversation
                        [["Hey... ssssssshhh!", "character-5-talking"],
                        ["Don't you know where you are?", "character-5"],
                        ["This is a library, not Unit 1.", "character-5-talking"],
                        ["Look around you... and listen...", "character-5"],
                        ["... busy students all working...", "character-5-talking"],
                        ["... and... silence...", "character-5"],
                        ["That's all I want...", "character-5"],
                        ["... and the ramen competition threatens that!", "character-5-talking"],
                        ["These kids will all stop working!", "character-5-talking"],
                        ["... and I can't have that.", "character-5"],
                        ["What was that?", "character-5-talking"],
                        ["I guess you're right...", "character-5"],
                        ["They do have to have fun sometimes...", "character-5-talking"],
                        ["Sorry?", "character-5"],
                        ["Well! If they love ramen as much as I love work!", "character-5"],
                        ["... I would never deprive them of that.", "character-5-talking"],
                        ["Here, take my clue...", "character-5-talking"]],
                    // 4th clue conversation
                        [["Woah, hold up there bud!", "character-4-talking"],
                        ["It's very busy around here!", "character-4"],
                        ["Sidwell street on a Wednesday! Chaos!", "character-4-talking"],
                        ["... hang on a second.", "character-4"],
                        ["You're Mr Tanaka's apprentice!", "character-4-talking"],
                        ["I'm cautious to give you the ingredient.", "character-4"],
                        ["I just want to make sure everyone's safe.", "character-4-talking"],
                        ["You may not think it, but it was crazy last year.", "character-4-talking"],
                        ["Devonshire folk love their ramen...", "character-4"],
                        ["Stampeding everywhere! I could barely move!", "character-4-talking"],
                        ["I'll give you your clue, but...", "character-4"],
                        ["... promise me, you'll help keep people safe.", "character-4-talking"],
                        ["We don't want a repeat of '97...", "character-4-talking"],
                        ["... that was traumatic...", "character-4", "character2", "character-1-drunk", "L"],
                        ["Yūto! Traffic cones are not toys!", "character-4-talking", "character2", "character-1-drunk", "L"]],
                    // 3rd clue conversation
                        [["I thought you'd be here sooner or later...", "character-3-talking"],
                        ["Why are we doing this?", "character-3"],
                        ["You seem like a nice kid...", "character-3-talking"],
                        ["Listen, we all have our reasons.", "character-3-talking"],
                        ["You see, I might not look like it...", "character-3"],
                        ["... but I'm the best barber in all of Exeter.", "character-3-talking"],
                        ["Even so, we have fewer customers nowadays.", "character-3"],
                        ["I tried to sponsor the ramen competition.", "character-3-talking"],
                        ["It would have been truly amazing for business.", "character-3-talking"],
                        ["... but they turned me down.", "character-3"],
                        ["No one turns me down!", "character-3"],
                        ["... I just want to be able to keep my shop.", "character-3-talking"],
                        ["That's all I care about. Have your clue...", "character-3"]],
                    // 2nd clue conversation
                        [["You found me!", "character-2-talking"],
                        ["I don't know how, but you found me!", "character-2"],
                        ["I didn't want this to happen...", "character-2-talking"],
                        ["I can't tell you the plan!", "character-2"],
                        ["... They said I wouldn't be able to help it.", "character-2-talking"],
                        ["... but Ramen Dayo can't win again!", "character-2-talking"],
                        ["Just solve my clue and you'll get it back.", "character-2"],
                        ["Before I accidentally tell you their plan!", "character-2-talking"]],
                    // 1st clue conversation
                        [["Hey! You must be with Mr Tanaka!", "character-1"],
                        ["My name is Yūto.", "character-1-talking"],
                        ["I might know where an ingredient is.", "character-1"],
                        ["I've been told not to tell you...", "character-1-talking"],
                        ["Let's make a deal...", "character-1"],
                        ["I'll think of a clue, and if you can solve it...", "character-1-talking"],
                        ["Then you can have your ingredient back.", "character-1"],
                        ["...", "character-1"],
                        ["Ah! I've got one!", "character-1-talking"]]
                    ];

/*createConvoArray();*/

var secondChar = false;
var convoPlace = 0;

window.onload = function (){
    var link = document.getElementById('guessLink');
    link.onclick = submitGuess;
}


function submitGuess() {
    document.getElementById('makeGuessForm').submit();
}

function createConvoArray() {
    var convoArray = [[]];
    for(var i=0;i<convos.length;i+2){
        var $speech = [convos[i],convos[i+1]];
        convoArray[0][0].push();
    }
    return convoArray;
}

function startConvo(textId, convo, convoId, charId) {
    convoId.style.display = "block";
    textId.innerHTML = conversations[convo][convoPlace][0];
    charId.src = "img/characters/"+conversations[convo][convoPlace][1]+".svg";
    convoPlace += 1;
}

function continueConvo(textId, convo, convoId, charId, bubbleId){
    textId.classList.remove("first-convo");
    textId.classList.remove("next-convo");
    void textId.offsetWidth;
    textId.classList.add("next-convo");
    if (convoPlace == conversations[convo].length) {
        convoPlace = 0;
        convoId.style.display = "none";
        if (document.getElementById('gInput') != null) {
            document.getElementById('gInput').style.display = "block";
        }
    } else {
        if (conversations[convo][convoPlace].length == 5){
            var secondCharId = document.getElementById(conversations[convo][convoPlace][2]);
            secondCharId.style.display = "block";
            secondCharId.src = "img/characters/"+conversations[convo][convoPlace][3]+".svg";
            secondChar = true;
            if (conversations[convo][convoPlace][4] == "R") {
                bubbleId.classList.remove("speech-bubble");
                bubbleId.classList.add("speech-bubble-right");
            } else {
                bubbleId.classList.remove("speech-bubble-right");
                bubbleId.classList.add("speech-bubble");
            }
        } else if(secondChar == true) {
            bubbleId.classList.remove("speech-bubble-right");
            bubbleId.classList.add("speech-bubble");
            secondChar = false;
            document.getElementById(conversations[convo][convoPlace - 1][2]).style.display = "none";
        }
        textId.innerHTML = conversations[convo][convoPlace][0];
        if (conversations[convo][convoPlace].length == 1){
            charId.src = "img/characters/empty.svg";
            bubbleId.classList.remove("speech-bubble-right");
            bubbleId.classList.remove("speech-bubble");
            bubbleId.classList.add("speech-bubble-none");
        } else {
            charId.src = "img/characters/"+conversations[convo][convoPlace][1]+".svg";
        }
        convoPlace += 1;
        //document.getElementById('voice-sound').play();
    }
}

function makeGuess(txtBoxId, correctAns) {
    if (txtBoxId.value.toUpperCase() === correctAns.innerHTML.toUpperCase()) {
        document.getElementById('clueForm').submit();
    } else {
        txtBoxId.classList.remove('wrong-answer');
        void txtBoxId.offsetWidth;
        txtBoxId.classList.add('wrong-answer');
    }
}
