<php

?>
<html lang="en">
    <head>
        <title>Administrator Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel='stylesheet' href="css/stylesheetLogin.css" type='text/css'>
        <link rel="stylesheet" href="https://use.typekit.net/xof2zdw.css">
        <link rel="icon" href="img/icon.png">
        <script src=""></script>
    </head>
    <body>
        <div class="infoContainer loginForm bg-transparent border-0">
            
            <h1>Admin Login</h1>
            <form>
                <label class="text-white">Only admins are able to log in through this portal.</label><br><br>
                <div class="form-group">
                    <input type="text" class="form-control text-center shadow login-input" id="unameInput" placeholder="Username">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control text-center shadow login-input" id="pwInput" placeholder="Password">
                </div>
                <?php
                    if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['errorMsg'])) {
                        echo("<p class='error'>".$_POST['errorMsg']."</p>");
                    }
                ?>
                <input type="submit" class="btn login-btn" value="Login">
            </form>
        </div>
        <div class="container" id="adminLogin"><a href="login.php" class="adminLink">Login as player</a></div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>