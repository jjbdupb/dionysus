<html lang="en">
    <head>
        <title>Hunt Dayo</title>
        <link rel="icon" href="img/icon.png">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel='stylesheet' href="css/stylesheetMain.css" type='text/css'>
        <link rel="stylesheet" href="https://use.typekit.net/xof2zdw.css">
        <script src="js/jss.js" type="text/javascript"></script></script>
    </head>

    <?php // if ($_POST['unameInput'] == NULL) {echo("<script type='text/javascript'>window.location.replace('login.php');</script>");}?>
        
        <?php if($_SERVER['REQUEST_METHOD'] == "GET") {
            $itemNo = $_GET['itemNo'];
            $convoNo = 9 + 4 - ((int)$itemNo); // number of clues - number of tanaka conversations
            echo("<body id='guess-body' onload='startConvo(bubbleTxt, ".$convoNo.", convo, character)'>");
            } else {echo("<body id='guess-body'>");}
        
            echo ("<div class='speech-overlay overlay' id='convo'>
                    <img class='character character-left' src='img/characters/character-".$itemNo.".svg' id='character'>
                    <img class='character character-right' src='img/characters/' id='character2'>
                    <div class='speech-bubble' id='bubble' data-parent='#bubbles'>
                        <p class='convo-text first-convo' id='bubbleTxt'></p>
                        <button class='convo-continue first-convo-btn shadow' id='bubbleBtn' onclick='continueConvo(bubbleTxt, ".$convoNo.", convo, character, bubble)'>Next</button>
                    </div>
                   </div>");
        
        
        
        
        ?>
        <!--<div class="back-div">
             clouds
            <img class="back-img" src="img/background.gif">
        </div>-->

        <?php
            $stageID = $_GET['stageID'];
            $stageReq = "GetStage|".$stageID;
            $queue = new ZMQSocket(new ZMQContext(), ZMQ::SOCKET_REQ);
            $queue->connect("tcp://controller:5555");
            $queue->send($stageReq);

            $stageStr = $queue->recv($clueReq);
            $stageArr = explode(',', $stageStr);

            $clue = $stageArr[3];
            $ans = $stageArr[4];

            echo("<div class='infoContainer card speech-style' id='gInput'>
                    <div class='form-group'>
                        <p class='guess-input-clue'>".$clue."</p>
                        <table class='w-100 text-center'><tbody>
                            <tr><td><input class='guess-input-text' type='text' id='guessBox'></td></tr>
                            <tr><td><button class='first-convo-btn shadow btn btn-primary' onclick='makeGuess(guessBox,clueAns)'>Submit Guess</button></td></tr>
                        </tbody></table>
                    </div>
                    <div class='d-none'>
                        <form action='index.php' method='post' id='clueForm'>
                            <input type='text' name='unameInput' value='".$_GET['unameInput']."' id='unameInput'>
                            <input type='text' name='emailInput' value='".$_GET['emailInput']."' id='emailInput'>
                            <input type='number' id='clueNo' name='clueNo' value='".$stageID."'>
                            <p id='clueAns'>".$ans."</p>
                        </form>
                    </div>
                    </div>");
                ?>
        <div class="outer-banner"><div class="inner-banner"></div></div> <!-- background div -->
        
        <!--<div class="conveyor">
        </div>-->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

</html>
