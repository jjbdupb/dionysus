Hello there. Welcome to 'Ramen Dayo!'
plain
The largest ramen restaurant in East Devon!
laughing
You must be my new apprentice! Nice to meet you!
happytalk
Welcome to your new home...
happy
Sorry for the wait, it's been very busy here.
talking
Everyone's preparing for the competition!
serioustalk
It's the biggest in the country...
talking
... and it's up coming very soon!
serioustalk
But I have to savour it this time...
sad
Why? I'm too old to be a ramen chef now.
serioustalk
I should be retiring soon.
sad
I can't keep up with these new places.
sadder
Besides...
crying
I have a lot of sleep to catch up on!
laughing
Anyway, I should show you around.
happytalk