<html lang="en">
    <head>
        <title>Hunt Dayo</title>
        <link rel="icon" href="img/icon.png">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel='stylesheet' href="css/stylesheetMain.css" type='text/css'>
        <link rel="stylesheet" href="https://use.typekit.net/xof2zdw.css">
        <script src="js/jss.js" type="text/javascript"></script>

        <!-- Leaflet (OpenSource Map API) -->
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
        <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
    </head>
    
    <?php if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['convoNo'])) {
        echo("<body id='index-body' onload='startConvo(bubble".$_POST['convoNo']."Txt, ".$_POST['convoNo'].", convo".$_POST['convoNo'].", character".$_POST['convoNo'].")'>");
    } else {echo("<body id='index-body'>");}
    ?>
    <?php if ($_POST['unameInput'] == NULL) {echo("<script type='text/javascript'>window.location.replace('login.php');</script>");}?>
        <script type="text/javascript">var jArray =<?php echo json_encode($convoArray); ?>;</script>
        <nav class="navbar navbar-expand-lg navbar-light shadow-lg">
            <a href="javascript:history.go(0)"><img class="navbarLogo" src="img/logos/dayo-white-horizontal.svg"></a>
            <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#pContainer" role="button" aria-expanded="true" aria-controls="pContainer">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#bContainer" role="button" aria-expanded="true" aria-controls="bContainer">Ingredients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#rContainer" role="button" aria-expanded="true" aria-controls="rContainer">Rules</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#aContainer" role="button" aria-expanded="true" aria-controls="aContainer">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#dContainer" role="button" aria-expanded="true" aria-controls="dContainer">Settings</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <a class="btn btn-outline-success my-2 my-sm-0" href="login.php">Sign Out</a>
                </form>
            </div>
        </nav>
        <div id="overlays">
            <div class="collapse overlay noTransition" id="dContainer" data-parent="#overlays">
                <div class="infoContainer shadow-lg">
                    
                    <div class="card card-body speech-style">
                        <h1>Debugger</h1>
                        <form action="index.php" method="post">
                            <label for="#convoInput" class="form-text text-muted" id="convoHelp">Conversation Starter</label>
                            <input type="number" id="convoNo" name="convoNo">
                            <input type="submit" name="convoSubmit" value="Submit">
                        </form>

                        <form action="index.php" method="post">
                            <label for="#convoInput" class="form-text text-muted" id="convoHelp">Get Item</label>
                            <input type="number" id="clueNo" name="clueNo" min="1" max="9">
                            <input type="submit" name="clue1" value="Submit" />
                        </form>
                        <form action="index.php" method="post">
                            <label for="#convoInput" class="form-text text-muted" id="convoHelp">Change Progress</label>
                            <input type="number" id="ingredientNo" name="ingredientNo" min="1" max="9">
                            <input type="submit" name="ingredient1" value="Submit" />
                        </form>
                        <table class="w-100">
                            <tbody>
                                <tr>
                                    <td class="w-50 align-middle text-right"><button onclick="setMusic()" id="setting-btn"><img id="music-setting" src="img/checked.svg"></button></td>
                                    <td class="w-50 align-middle text-left"><p class="mb-0">Play Music</p></td>
                                </tr>
                            </tbody>
                        </table>
                        <script type="text/javascript">
                            var playing = true;
                            function setMusic() {
                                var player = document.getElementById('bg-music');
                                var check = document.getElementById('music-setting');

                                if (playing == true) {
                                    check.src = "img/unchecked.svg";
                                    playing = false;
                                    player.pause();                                    
                                } else if (playing == false) {
                                    check.src = "img/checked.svg";
                                    playing = true;
                                    player.play();
                                }
                            }
                        </script>
                    </div>
                    
                    
                </div>
            </div>
            <div class="collapse overlay noTransition" id="pContainer" data-parent="#overlays">
                <div class="infoContainer shadow-lg">
                    
                    <div class="card card-body speech-style">
                        <h1>Profile</h1>
                        <img class="profile-pic" src="img/egg.svg">
                        <?php
                            echo("<p>@".$_POST['unameInput']."'s account:<br><br>Email: ".$_POST['emailInput']."</p>
                                    <form id='deleteMe' action='utils/deleteAccount.php' method='POST'>
                                        <input type='text' name='delAccount' value='".$_POST['unameInput']."' class='d-none'>
                                        <input type='text' name='deleter' value='".$_POST['unameInput']."' class='d-none'>
                                    </form>
                                    <button class='btn btn-primary' id='delAccBtn'>Delete Account</button>
                                    <script type='text/javascript'>
                                        var link = document.getElementById('delAccBtn');
                                        link.onclick = confirmDelete;

                                        function confirmDelete() {
                                            if (confirm('Do you want to submit?')) {
                                                document.getElementById('deleteMe').submit();
                                            }
                                        }
                                    </script>");
                        ?>
                    </div>
                    
                    
                </div>
            </div>

            <div class="collapse overlay noTransition" id="rContainer" data-parent="#overlays">
                
                <div class="infoContainer shadow-lg">
                    <div class="card card-body speech-style">
                        <h1>Rules</h1>
                        <div class="rules-container overflow-auto">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus mattis eleifend. Quisque
                                bibendum mollis mi, sed finibus sapien aliquam at. Sed ex justo, tempor a posuere id, pulvinar
                                vitae purus. Nam convallis massa sit amet sapien varius, vestibulum scelerisque orci mattis.
                                Proin convallis nisi justo, in mattis sem tempor quis. Maecenas ut ante sed ante ornare blandit.
                                Vestibulum finibus elementum est at laoreet. Phasellus ipsum lorem, consectetur sed dapibus in,
                                efficitur id dolor.</p>
                            <p>Etiam sit amet egestas arcu, non lacinia nisi. Proin velit ex, viverra in eleifend vel, bibendum
                                vel massa. Nulla nec risus orci. Nullam ornare faucibus venenatis. Duis hendrerit nisi eu ornare
                                bibendum. Nulla pretium augue quis elit efficitur molestie. Cras eget lectus hendrerit nunc
                                feugiat eleifend aliquam in ipsum. Curabitur ultrices ultrices magna, elementum commodo
                                libero bibendum nec. Nam vel sodales metus, et pellentesque nunc. Quisque vulputate quam varius
                                quam pharetra tempus. Ut vitae tellus velit.</p>
                            <p>Sed eget tortor dignissim, vulputate massa mattis, porta mi. Curabitur finibus nunc a dui gravida,
                                ut luctus dui pulvinar. Sed id molestie quam, eu interdum velit. Proin venenatis ullamcorper leo
                                ac aliquam. Fusce eget ligula aliquam lorem mattis sollicitudin. Sed euismod elit quis massa
                                condimentum, vel convallis justo ultricies. Vivamus elit purus, tempus id porta at, faucibus
                                vitae enim. Aliquam ut nulla ex. Vestibulum bibendum massa in consequat tincidunt.</p>
                            <p>Donec sit amet elit facilisis, malesuada nibh sit amet, imperdiet nibh. Donec ac ipsum a leo
                                facilisis pellentesque auctor a risus. Nunc rhoncus volutpat commodo. Nam iaculis finibus ante,
                                ac blandit massa vulputate id. Phasellus viverra lobortis quam id dignissim. Aliquam sed auctor
                                ligula, ut finibus orci. Praesent in facilisis neque. Donec quis arcu eu urna finibus interdum.
                                Maecenas malesuada at metus gravida pellentesque.</p>
                            <p>Nulla auctor, massa sed auctor volutpat, elit nulla fringilla dui, sit amet porttitor lorem ex
                                quis arcu. Aenean in orci magna. Cras mi massa, molestie eget lectus ut, maximus dignissim ipsum.
                                Donec non egestas augue. Morbi volutpat, velit vitae aliquet tempor, nisi sapien congue nisl,
                                a condimentum dolor erat ut sem. Sed felis mauris, egestas id mi sed, rhoncus luctus massa.
                                Morbi dictum finibus velit sodales lobortis. Vivamus consequat neque vel nisi dictum, non tempus
                                nunc molestie. Suspendisse nulla sem, aliquet sed facilisis vitae, sodales nec eros.</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="collapse overlay noTransition" id="aContainer" data-parent="#overlays">
                
                <div class="infoContainer shadow-lg">
                    <div class="card card-body speech-style">
                        <h1>About</h1>
                        <div id="accordion"> <div class="card"> <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    FAQs
                                </button>
                            </h5>
                        </div>
                        
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                FAQs
                            </div>
                        </div>
                    </div>
                    <div class="card"> <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Development
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion"> <div class="card-body">
                            Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> and <a href="https://www.flaticon.com/authors/dave-gandy">Dave Gandy</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
                    </div></div></div></div></div>
                </div>
            </div>
            <?php

                $ingredients = array("Rice", "Salt", "Chilli", "Soy Sauce", "Sake", "Egg", "Dimsum", "Chopsticks", "Chicken");
                
                $IDrequest = "GetUserFromUserName|".$_POST['unameInput'];
                $queue = new ZMQSocket(new ZMQContext(), ZMQ::SOCKET_REQ);
                $queue->connect("tcp://controller:5555");
                $queue->send($IDrequest);

                $userStr = $queue->recv();
                $userArr = explode(',', $userStr);
                $userID = $userArr[0];
                $stageID = $userArr[5];
                $playerStage = $userArr[5];

                if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['convoNo'])) {     // If a conversation is supposed to start
                    echo("
                        <div class='speech-overlay overlay' id='convo".$_POST['convoNo']."'>
                            <img class='character character-left' src='img/characters/tanaka-plain.svg' id='character".$_POST['convoNo']."'>
                            <img class='character character-right' src='img/characters/' id='character2'>
                            <div class='speech-bubble' id='bubble".$_POST['convoNo']."' data-parent='#bubbles'>
                                <p class='convo-text first-convo' id='bubble".$_POST['convoNo']."Txt'></p>
                                <button class='convo-continue first-convo-btn shadow' id='bubble".$_POST['convoNo']."Btn' onclick='continueConvo(bubble".$_POST['convoNo']."Txt, ".$_POST['convoNo'].", convo".$_POST['convoNo'].", character".$_POST['convoNo'].", bubble".$_POST['convoNo'].")'>Next</button>
                            </div>
                        </div>");
                }
                if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['clueNo'])) {     // If the player is loading the page as they just solved a clue
                    echo("<div class='ingredient-got'>                                     
                            <img class='ingredient-new' src='img/ingredients/clue".$_POST["clueNo"]."-found.svg'>
                            <h1 class='ingredient-title'>You found an ingredient!</h1>
                            <img class='fan' src='img/fan.png'>
                        </div>");
                    $playerStage = $_POST['clueNo'];

                    // Process clue solved here
                    $stageIDreq = "GetStageIDFromName|".$ingredients[$_POST['clueNo']];
                    $queue->send($stageIDreq);
                    $stageID = $queue->recv();

                    $newStageReq = "AddStageComplete|".$userID."|".$stageID;
                    $queue->send($newStageReq);
                    $acc = $queue->recv();
                    $newCurrentStage = "UpdateUser|".$userID."|CurrentStage|".$stageID;
                    $queue->send($newCurrentStage);
                    $acc = $queue->recv();

                }

                $userData = "GetUserFromUserID|".$userID;
                $queue->send($userData);               
                $userStr = $queue->recv();
                $userArr = explode(',', $userStr);
                $userID = $userArr[0];
                $stageID = $userArr[5];
                $playerStage = $userArr[5] - 1;
                $cluesNo = count($ingredients);
                $percent = ($playerStage*100)/$cluesNo;

                $output = "<div class='collapse overlay noTransition' id='bContainer' data-parent='#overlays'>
                                <div class='ingredient-list shadow fixed-bottom'>
                                    <h1 class='ingredient-h'>Ingredients</h1><div class='progress'>
                                    <div class='progress-bar' role='progressbar' style='width:".$percent."%' aria-valuenow='".$percent."' aria-valuemin='0' aria-valuemax='100'></div></div>
                                        <div class='badges-container'>
                                            <table class='badge-table'>
                                                <tbody>";
                        
                for ($i = 1; $i <= 9; $i++) {
                    if (($i - 1) % 3 == 0) {
                        $output.="<tr>";
                    }
                    if($i <= $playerStage){
                        $output.="<td><div class='card card-body clue-card clue-found'>
                                    <img class='circle' src='img/circle.svg'>
                                    <img class='clue-badge badge-found' src='img/ingredients/clue".$i."-found.svg' id='clue".$i."'>
                                    <p>".$ingredients[$i-1]."</p></div></td>";
                    } elseif($i == $playerStage + 1) {
                        $output.="<td><div class='card card-body clue-card clue-found'>
                                    <a href='#' id='guessLink'><span class='link-div'></span></a>
                                    <form class='d-none' id='makeGuessForm' action='makeGuess.php' method='GET'>
                                        <input type='text' name='unameInput' value='".$_POST['unameInput']."' id='unameInput'>
                                        <input type='text' name='emailInput' value='".$_POST['emailInput']."' id='emailInput'>
                                        <input type='number' name='itemNo' value='".$i."'>
                                        <input type='number' name='stageID' value='".$stageID."'>
                                    </form>
                                    <img class='circle' src='img/circle.svg'>
                                    <img class='clue-badge badge-found' src='img/ingredients/clue".$i."-hidden.svg' id='clue".$i."'>
                                    <p>Next Clue</p>
                                    <script type='text/javascript'>
                                            var link = document.getElementById('guessLink');
                                            link.onclick = submitGuess;
                                    </script></div></td>";
                    } else {
                        $output.="<td><div class='card card-body clue-card clue-hidden'>
                                    <img class='clue-badge badge-hidden' src='img/ingredients/clue".$i."-hidden.svg' id='clue".$i."'>
                                    <p>".$ingredients[$i-1]."</p></div></td>";
                    }
                    if ($i % 3 == 0) {
                        $output.="</tr>";
                    }
                }

                $output.="<tr>
                            <td></td>
                            <td><div class='card card-body clue-card clue-found'>";
                if ($playerStage >= 9) {
                    $output.="<a href='finalClue.php'><span class='link-div'></span></a>
                                <img class='circle' src='img/circle.svg'>";
                }

                $output.="
                            <img class='clue-badge badge-found' src='img/final.svg' id='clue10'>
                            <p id='final-txt'>Enter the Competition!</p></div></td>
                        <td></td>
                    </tr></tbody></table></div></div></div></div>";
                        
                echo($output);
            ?>
            
            <!--<audio id="voice-sound" src="misc/voices.mp3" preload="auto"></audio>-->
        
        <div id="map" class="container mainMap">
            <h1>Not logged in</h1>
        </div>
        <script src="js/map.js" type="text/javascript"></script>

        <?php
            // Map Initialization
            echo("<script>");
            echo("var mainMap = Map.initializeMap(\"" . $userID . "\");");
            echo("</script>");
        ?>
        
        <audio controls loop autoplay id="bg-music" class="d-none">
            <source src="misc/bground.mp3" preload="auto">
        </audio>
        <!--<iframe class="d-none"src="misc/bground.mp3" allow="autoplay">-->
        <script type="text/javascript">document.getElementById('bg-music').play();</script>
    
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

</html>
