<html lang="en">
    <head>
        <title>Hunt Dayo</title>
        <link rel="icon" href="img/icon.png">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel='stylesheet' href="css/stylesheetMain.css" type='text/css'>
        <link rel="stylesheet" href="https://use.typekit.net/xof2zdw.css">
        <script src="js/jss.js" type="text/javascript"></script></script>
    </head>
        <body id='guess-body' onload='startConvo(bubbleTxt, 2, convo, character)'>
        <!-- ***** Need to add some check as players can very easily change the GET variable in the url and advance in the game ***** -->
        <?php
        
            echo ("<div class='speech-overlay overlay' id='convo'>
                    <img class='character character-left' src='img/characters/tanaka-talking' id='character'>
                    <img class='character character-right' src='img/characters/' id='character2'>
                    <div class='speech-bubble' id='bubble' data-parent='#bubbles'>
                        <p class='convo-text first-convo' id='bubbleTxt'></p>
                        <button class='convo-continue first-convo-btn shadow' id='bubbleBtn' onclick='continueConvo(bubbleTxt, 2, convo, character, bubble)'>Next</button>
                    </div>
                   </div>");
        
        
        
        
        ?>

        <?php
            echo("<div class='infoContainer card speech-style' id='gInput'>
                    <div class='form-group'>
                        <p class='guess-input-clue'>What was the university's name changed to in 1900?</p>
                        <table class='w-100'><tbody>
                            <tr class='w-100'><td class='w-100 text-center'><input class='guess-input-text m-auto' type='text' id='guessBox'></td></tr>
                            <tr class='w-100'><td class='w-100 text-center'><button class='first-convo-btn shadow btn btn-primary' onclick='makeGuess(guessBox,clueAns)'>Submit Guess</button></td></tr>
                        </table></tbody>
                    </div>
                    <div class='d-none'>
                        <form action='index.php' method='post' id='clueForm'>
                            <input type='number' id='clueNo' name='clueNo' value='10'>
                            <p id='clueAns'>Royal Albert Memorial College</p>
                        </form>
                    </div>
                    </div>");
                ?>
        <div class="outer-banner"><div class="inner-banner"></div></div> <!-- background div -->
        
        <!--<div class="conveyor">
        </div>-->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

</html>