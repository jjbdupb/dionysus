<html lang="en">
    <head>
        <title>Creating account...</title>
        <link rel="icon" href="../img/icon.png">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>

        <form class="d-none" id="errorForm" action="../login.php" method="POST">
            <input type="text" value="* Username or password incorrect" name="errorMsg" id="errorMsg">
            <input type="submit" value="submit">
        </form>
        <form class="d-none" id="successForm" action="../index.php" method="POST">
            <input type="text" value="unameInput" name="unameInput" id="uname">
            <input type="text" value="emailInput" name="emailInput" id="email">
            <input type="submit" value="submit">
        </form>

        <?php
            $queue = new ZMQSocket(new ZMQContext(), ZMQ::SOCKET_REQ);
            $queue->connect("tcp://controller:5555");
                $userReq = "GetUserFromUserName|".$_POST['unameInput'];
                $queue->send($userReq);
                $userStr = $queue->recv();
                echo("<script type='text/javascript'>document.getElementById('errorMsg').value=".$userStr.";</script>");
                $userArr = explode(',', $userStr);
                
                if (in_array($_POST['unameInput'], $userArr) and password_verify($_POST['pwInput'], $userArr[3])) {
                    $loginStatus = TRUE; // Check if valid account
                } else { $loginStatus = FALSE; }

                if ($loginStatus == TRUE) {
                    // If message successful
                    echo("<script type='text/javascript'>
                            document.getElementById('uname').value = '".$_POST['unameInput']."';
                            document.getElementById('email').value = '".$_POST['emailInput']."';
                            document.getElementById('successForm').submit();
                        </script>");
                    
                } elseif($loginStatus == FALSE) {
                    echo("<script type='text/javascript'>
                            document.getElementById('errorForm').submit();
                        </script>");
                } 
            
        ?>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

</html>