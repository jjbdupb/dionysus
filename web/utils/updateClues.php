<html lang="en">
    <head>
        <title>Updating Clues...</title>
        <link rel="icon" href="../img/icon.png">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
        
        <?php
            $stageName = array("Rice", "Salt", "Chilli", "Soy Sauce", "Sake", "Egg", "Dimsum", "Chopsticks", "Chicken");
            $queue = new ZMQSocket(new ZMQContext(), ZMQ::SOCKET_PUB);
            $queue->connect("tcp://controller:5555");

            for ($i = 0; $i < $_POST['noClues']; $i++) {
                $stageIDreq = "GetStageIDFromName|".$stageName[$i];
                $queue->send($stageIDreq);
                $stageID = $queue->recv();

                $queue->send("UpdateStage|".$stageID."|Description|".$_POST['descr-'.$i]);
                $queue->send("UpdateStage|".$stageID."|Clue|".$_POST['clue-'.$i]);
                $queue->send("UpdateStage|".$stageID."|Solution|".$_POST['sol-'.$i]);
                $queue->send("UpdateStage|".$stageID."|Longitude|".$_POST['long-'.$i]);
                $queue->send("UpdateStage|".$stageID."|Latitude|".$_POST['latit-'.$i]);
                $queue->send("UpdateStage|".$stageID."|Cluster|".$_POST['clust-'.$i]);
            }
        ?>
        
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

</html>