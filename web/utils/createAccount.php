<?php
    function checkInputs($uname, $pwd, $pwd2) {
        $status = "good";
        if (strcmp($uname, "") !== 0) {
            if (strcmp($pwd, $pwd2) !== 0) {
                $status = 'diffPwds';
            } elseif (strlen($pwd) < 8) {
                // Validate password strength
                $uppercase = preg_match('@[A-Z]@', $pwd);
                $lowercase = preg_match('@[a-z]@', $pwd);
                $number    = preg_match('@[0-9]@', $pwd);
                echo($uppercase." ".$lowercase." ".$number." ".strlen($pwd));
                if(!$uppercase || !$lowercase || !$number || strlen($pwd) < 8) {
                    $status='weakPwd';
                }
            }
        } else { $status = 'fillAll'; }
        return $status;
    }
?>

<html lang="en">
    <head>
        <title>Creating account...</title>
        <link rel="icon" href="../img/icon.png">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>

        <form class="d-none" id="errorForm" action="../signUp.php" method="POST">
            <input type="text" value="good" name="errorMsg" id="errorMsg">
            <input type="submit" value="submit">
        </form>
        <form class="d-none" id="successForm" action="../index.php" method="POST">
            <input type="text" value="uname" name="unameInput" id="uname">
            <input type="text" value="email" name="emailInput" id="email">
            <input type="number" value="0" name="convoNo" id="convoNo"> <!-- Starts introduction with Tanaka -->
            <input type="submit" value="submit">
        </form>

        <?php
            if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['unameInput']) and isset($_POST['emailInput']) and isset($_POST['pwInput']) and isset($_POST['rePwInput'])) {

                $dataStatus = checkInputs($_POST['unameInput'], $_POST['pwInput'], $_POST['rePwInput']);

                if ($dataStatus == 'good') {
                    $hashedPwd = password_hash($_POST['pwInput'], PASSWORD_DEFAULT);
                    $userData = "AddUser|".$_POST['unameInput']."|".$_POST['emailInput']."|".$hashedPwd."|0";
                        
                    $queue = new ZMQSocket(new ZMQContext(), ZMQ::SOCKET_REQ);
                    $queue->connect("tcp://controller:5555");
                    $queue->send($userData);
                    // If message successful
                    echo("<script type='text/javascript'>
                            document.getElementById('uname').value = '".$_POST['unameInput']."';
                            document.getElementById('email').value = '".$_POST['emailInput']."';
                            document.getElementById('successForm').submit();
                        </script>");
                    
                } elseif($dataStatus == 'diffPwds') {
                    echo("<script type='text/javascript'>
                            document.getElementById('errorMsg').value = '* Passwords don't match.';
                            document.getElementById('errorForm').submit();
                        </script>");
                } elseif($dataStatus == 'weakPwd') {
                    echo("<script type='text/javascript'>
                            document.getElementById('errorMsg').value = '* Password does not meet requirements.';
                            document.getElementById('errorForm').submit();
                        </script>");
                } elseif($dataStatus == 'fillAll') {
                    echo("<script type='text/javascript'>
                            document.getElementById('errorMsg').value = '* Please fill all the boxes.';
                            document.getElementById('errorForm').submit();
                        </script>");
                }
            }

        ?>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

</html>