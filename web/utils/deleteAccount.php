<html lang="en">
    <head>
        <title>Deleting account...</title>
        <link rel="icon" href="../img/icon.png">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
        <?php
            $queue = new ZMQSocket(new ZMQContext(), ZMQ::SOCKET_REQ);
            $queue->connect("tcp://controller:5555");
            $queue->send("GetUserFromUserName|".$_POST['delAccount']);

            $userArr = explode(',', $queue->recv());
            $userID = $userArr[0];
            $queue->send("RemoveUser|".$userID);

            if ($_POST['delAccount'] != $_POST['deleter']) { // If game keeper deleting profile
                echo("<form class='d-none' id='gk-success' action='../gkMain.php' method='POST'>
                        <input type='text' name='uname' value='".$_POST['deleter']."' id='uname'>
                        <input type='text' name='gkShowPanel' value='keepersCont' id='gkShowPanel'>
                    </form>
                    <script type='text/javascript'>
                        document.getElementById('gk-success').submit();
                    </script>");
            } else {
                header("Location: ../login.php");
                exit();
            }
            
        ?>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

</html>
