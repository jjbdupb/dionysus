<html lang="en">
    <head>
        <title>Create Account</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel='stylesheet' href="css/stylesheetLogin.css" type='text/css'>
        <link rel="stylesheet" href="https://use.typekit.net/xof2zdw.css">
        <link rel="icon" href="img/icon.png">
        <script src=""></script>
    </head>
    <body>
        <div class="card card-body infoContainer loginForm shadow-lg">
            <h1>Sign Up</h1>
            <form action="utils/createAccount.php" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control text-center login-input" id="unameInput" name="unameInput" placeholder="Username">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control text-center login-input" id="emailInput" name="emailInput" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control text-center login-input" id="pwInput" name="pwInput" aria-describedby="pwdHelp" placeholder="Password">
                    <small class="form-text text-muted" id="pwdHelp">Password must be at least 8 characters and contain a mix of numbers and upper and lowercase letters.</small>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control text-center login-input" id="rePwInput" name="rePwInput" placeholder="Re-type Password">
                </div>
                <?php
                    if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['errorMsg'])) {
                        echo("<p class='error'>".$_POST['errorMsg']."</p>");
                    }
                ?>
                <table class="w-100 text-center">
                    <tbody>
                        <tr><td><input type="submit" class="btn login-btn" value="Create Account"></td></tr>
                        <tr><td><a class="btn login-btn" href="login.php">Cancel</a></td></tr>
                    </tdbody>
                </table>
            </form>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
