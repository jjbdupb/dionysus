#! /bin/bash
set -e
set -x
DOWN="sudo docker-compose -f /opt/dionysus/docker-compose.yml down "
UP="sudo nohup docker-compose -f /opt/dionysus/docker-compose.yml up &> /var/log/dionysus.log &"
STOP="sudo docker-compose -f /opt/dionysus/docker-compose.yml stop"

${DOWN}

${UP}
wait 60
${STOP}
${UP}