#ifndef __MESSAGEHANDLER__
#define __MESSAGEHANDLER__

#include <iostream>
#include <vector>
#include <string>

#include <zmq.hpp>
#include <unistd.h>
#include "databaseInterface.hpp"
#include <boost/lexical_cast.hpp>


/**
* @brief Fucntion to split out arguments from single string with each element seperated by
* 		 a delimiter, defaults to |
* @param str : String to split into arguments
* @param delim : Delemiter seperating arguments in str, defaults to |
* @return reslt : vector of strings represnting each argument
*/
std::vector<std::string> getArgs(const std::string &str, const std::string delim = "|");


/**
* @brief Class to handle messages, contains relevent functions to format given arguments from zmq
*		 into database understanable formats
*/
class messageHandler
{
public:
    Database *db;

    /**
    * @brief constructor for messageHandler class, creates nececesery connection to specifed database
    * @param conf_file : file path to config file contaiing database information
    */
    messageHandler(std::string conf_file);


    /**
    * @brief Handles Adding a Stage to database
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string to return to client
    */
    std::string handleAddStage(const std::string &data);

    /**
    * @brief Handles Adding a updating a stage in the database
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string to return to client
    */
    std::string handleUpdateStage(const std::string &data);

    /**
    * @brief Handles getting the current stage a user is on
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string containing stage information
    */
    std::string handleGetCurrentUserStage(const std::string &data);

    /**
    * @brief Handles updating a user information in the database
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string to return to client
    */
    std::string handleUpdateUser(const std::string &data);

    /**
    * @brief Handles getting a list of all users and their information
    * @return string of all user information to return to cient, each user
    *		   is seperated by a |
    */
    std::string handleGetUserList();

    /**
    * @brief Handles getting user information from UserName
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string containing user information to return to client
    */
    std::string handleGetUserFromUserName(const std::string &data);

    /**
    * @brief Handles getting user information from user ID
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string containing user information to return to client
    */
    std::string handleGetUserFromUserID(const std::string &data);

    /**
    * @brief Handles Adding a user to the database
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string to return to client
    */
    std::string handleAddUser(const std::string &data);

    /**
    * @brief Handles Adding a completed stage to the database
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string to return to client
    */
    std::string handleAddStageComplete(const std::string &data);

    /**
    * @brief Handles getting a users completed stages
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string containing the completed stages of a user
    */
    std::string handleGetUserCompleteStages(const std::string &data);

    /**
    * @brief Handles removing a user form the database
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string to return to client
    */
    std::string handleRemoveUser(const std::string &data);

    /**
    * @brief Handles getting a stageID from stage Name
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string containing stage ID to return to client
    */
    std::string handleGetStageIDFromName(const std::string &data);

    /**
    * @brief Handles Agetting a stage's information
    * @param data : Raw data string passed to Handler from ZMQ
    * @return string containing stage information to return to client
    */
    std::string handleGetStage(const std::string &data);

    /**
     * @breif Handles removing a stage from the database
     * @param data: raw data string passed to Handler from ZMQ
     * @return string containing succsess information
    */
    std::string handleRemoveStage(const std::string &data);

    /**
    * @brief Handles resolving a message, i.e. exercuting the requestion function
    *		  specifed in the zmq message
    * @param message : Raw zmq::messaeg_t that contains all needed information to
    *					select the correct funtion, and needed informated to exercute said funciton
    * @return string to return to client
    */
    std::string resolveMessage(zmq::message_t &message);
};
#endif // __MESSAGEHANDLER__