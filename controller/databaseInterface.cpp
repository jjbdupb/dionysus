#include "databaseInterface.hpp"

Database::Database(std::string conf_file)
{
    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini(conf_file, pt);
    dbName = pt.get<std::string>("Database.dbName");
    user = pt.get<std::string>("Database.user");
    password = pt.get<std::string>("Database.password");
    host = pt.get<std::string>("Database.host");
    port = pt.get<std::string>("Database.port");
    maxconnect = pt.get<int>("Handler.dbConnectAttemps");
    attempts = 1;
    waitTime = pt.get<int>("Handler.dbWaitTime");}

bool Database::connect()
{
    std::string connect = fmt::format("dbname={} user={} password={} host={} port={} connect_timeout={}", dbName, user, password, host, port, timeout);
    while(attempts <= maxconnect){
        try{
            db = new pqxx::connection(connect);
            break;
        }catch(...){
            std::cout<< "Could not Connect to database... Waiting 30's then trying again: Attempt " << attempts << " of " << maxconnect<<std::endl;
            attempts+=1;
            sleep(waitTime);

        }
    }
    if(attempts == maxconnect){
        std::cout << "Could not connect to database : limit reached" <<std::endl;
        exit(-1);
    }

    if (db->is_open()) {
        std::cout << "Connected to Database Dionysus" << std::endl;
    } else {
        std::cout << "ERROR: Could Not connect to Database" << std::endl;
        return false;
    }

    return true;

}

bool Database::addStage(std::string name, std::string description, std::string clue, std::string solution, float longitude, float latitude, std::string cluster)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("INSERT INTO STAGES (StageID,StageName,Description,Clue,Solution,Longitude,Latitude,Cluster) "\
                                  "VALUES (DEFAULT, '{}', '{}', '{}', '{}', {}, {}, '{}');", name, description, clue, solution, longitude, latitude, cluster);
    w.exec(sql);
    w.commit();

    return true;
}

bool Database::updateStage(int stageID, std::string field, std::string value)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("UPDATE STAGES SET {} = '{}' WHERE StageID = {};", field, value, stageID);

    w.exec(sql);
    w.commit();

    return true;
}

bool Database::addUser(std::string username, std::string email, std::string password, bool adminFlag)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("INSERT INTO Users (UserID, UserName, Email, PasswordHash, AdminFlag, CurrentStage) "\
                                  "VALUES (DEFAULT, '{}','{}','{}','{}',1);", username, email, password, adminFlag);

    w.exec(sql);
    w.commit();
    return true;
}

std::string Database::getUserFromUserName(std::string username)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("SELECT * FROM Users WHERE UserName = '{}';", username);
    pqxx::result rst = w.exec(sql);

    pqxx::row const row = rst[0];
    std::string output = fmt::format("{},{},{},{},{},{}", row[0].c_str(), row[1].c_str(), row[2].c_str(), row[3].c_str(), row[4].c_str(), row[5].c_str());

    return output;
}

std::string Database::getUserFromUserID(std::string userID)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("SELECT * FROM Users WHERE UserID = '{}';", userID);
    pqxx::result rst = w.exec(sql);

    pqxx::row const row = rst[0];
    std::string output = fmt::format("{},{},{},{},{},{}", row[0].c_str(), row[1].c_str(), row[2].c_str(), row[3].c_str(), row[4].c_str(), row[5].c_str());

    return output;
}

int Database::getCurrentUserStage(std::string userID)
{

    pqxx::work w(*db);
    std::string sql = fmt::format("SELECT CurrentStage FROM Users WHERE UserID = '{}'", userID);

    pqxx::result rst = w.exec(sql);
    pqxx::row const row = rst[0];
    int stage = row[0].as<int>();
    return stage;
}

bool Database::updateUser(std::string userID, std::string field, std::string value)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("UPDATE USERS SET {} = '{}' WHERE UserID = '{}';", field, value, userID);


    w.exec(sql);
    w.commit();

    return true;
}

std::string Database::getUserList()
{
    pqxx::work w(*db);
    std::string sql = fmt::format("SELECT * FROM Users;");

    pqxx::result rst = w.exec(sql);
    std::string output = "";

    for (int i = 0; i < rst.size(); ++i) {
        pqxx::row const row = rst[i];
        std::string rowInfo = fmt::format("{},{},{},{},{},{}|", row[0].c_str(), row[1].c_str(), row[2].c_str(), row[3].c_str(), row[4].c_str(), row[5].c_str());
        output = output + rowInfo;
    }

    return output;
}

std::string Database::getStage(int stageID)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("SELECT * FROM STAGES WHERE StageID = '{}';", stageID);
    pqxx::result rst = w.exec(sql);

    pqxx::row const row = rst[0];

    std::string output = fmt::format("{},{},{},{},{},{},{},{}", row[0].c_str(), row[1].c_str(), row[2].c_str(), row[3].c_str(), row[4].c_str(), row[5].c_str(), row[6].c_str(), row[7].c_str());

    return output;
}


bool Database::addStageComplete(std::string userID, int stageID)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("INSERT INTO StagesCompleted (UserID, StageID) VALUES('{}', {});", userID, stageID);

    w.exec(sql);
    w.commit();

    return true;
}

std::string Database::getUserCompleteStages(std::string userID)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("SELECT StageID FROM StagesCompleted WHERE UserID = '{}';", userID);

    pqxx::result rst = w.exec(sql);
    std::string output;

    for (int i = 0; i < rst.size(); ++i) {
        pqxx::row const row = rst[i];
        std::string rowInfo = fmt::format("{}|", row[0].c_str());
        output = output + rowInfo;
    }

    return output;
}

int Database::getStageIDFromName(std::string stageName)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("SELECT StageID FROM Stages WHERE StageName = '{}';", stageName);
    pqxx::result rst = w.exec(sql);

    pqxx::row const row = rst[0];
    int id = row[0].as<int>();

    return id;
}

bool Database::removeUser(std::string UserID)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("DELETE FROM StagesCompleted WHERE UserID = '{}';", UserID);
    w.exec(sql);
    sql = fmt::format("DELETE FROM Users WHERE UserID = '{}';", UserID);
    w.exec(sql);
    w.commit();

    return true;
}

bool Database::removeStage(int stageID)
{
    pqxx::work w(*db);
    std::string sql = fmt::format("DELETE FROM Stages WHERE StageID = '{}';", stageID);
    w.exec(sql);
    w.commit();

    return true;

}


// On move
Database::Database(Database &&other)
{
    this->db = other.db;
    other.db = nullptr;
}

Database::~Database()
{
    if (db) {
        // Does 'db' need closing?
        delete db;

    }
}
