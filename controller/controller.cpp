#include <string>
#include <iostream>
#include <zmq.hpp>
#include <stdlib.h>
#include <pqxx/pqxx>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include "MessageHandler.hpp"

int main(int argc, char const *argv[])
{
    //  Prepare our context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REP);
    socket.bind ("tcp://*:5555");
    boost::property_tree::ptree pt;
    try{
    boost::property_tree::ini_parser::read_ini(argv[1], pt);
    }catch(...){
        std::cout << "Could not read conf file:" << "\n" << "Exiting ...." <<std::endl;
        exit(-1);
    }
    messageHandler handlr(argv[1]);

    std::cout << "Launching Controller" << std::endl;
    while (true) {
        zmq::message_t request;

        //  Wait for next request from client
        socket.recv (&request);
        std::string acc;
        try{
            acc = handlr.resolveMessage(request);
        }catch(const std::exception &exc){
            std::cerr << exc.what();
            acc = "ERROR_INVALID_MESSAGE";
        }
        std::cout<<"Sending Message:" << acc << std::endl;
        socket.send(zmq::buffer(acc), zmq::send_flags::dontwait);
    }

    return 0;
}