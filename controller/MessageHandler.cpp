#include "MessageHandler.hpp"


std::vector<std::string> getArgs(const std::string &str, const std::string delim)
{
    size_t pos_start = 0, pos_end, delim_len = delim.length();
    std::string tmp;
    std::vector<std::string> reslt;
    while ((pos_end = str.find(delim, pos_start)) != std::string::npos) {
        tmp = str.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        reslt.push_back(tmp);
    }

    reslt.push_back(str.substr(pos_start));
    return reslt;
}

messageHandler::messageHandler(std::string conf_file)
{
    db = new Database(conf_file);
    bool succ = db->connect();
    if(succ == false) {
        fprintf(stderr, "Could not connect to data base\n");
        exit(-1);
    }


}

std::string messageHandler::handleAddStage(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);
    std::string name = v[1], description = v[2], task = v[3], solution = v[4], cluster = v[7];
    float longitude = std::stof(v[5]), lat = std::stof(v[6]);
    bool succ = db->addStage(name, description, task, solution, longitude, lat, cluster);
    if(succ != true) {
        return "INTERNAL ERROR";
    }

    return "200";
}

std::string messageHandler::handleUpdateStage(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);

    std::string field = v[2], value = v[3];
    int stageID = std::stoi(v[1]);

    bool succ = db->updateStage(stageID, field, value);

    if(succ != true) {
        return "INTERNAL ERROR";
    }

    return "200";
}

std::string messageHandler::handleGetCurrentUserStage(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);

    std::string userID = v[1];

    int num = db->getCurrentUserStage(userID);

    return std::to_string(num);
}

std::string messageHandler::handleUpdateUser(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);

    std::string userID = v[1], field = v[2], value = v[3];

    bool succ = db->updateUser(userID, field, value);

    if (succ != true) {
        return "INTERNAL ERROR";
    }

    return "200";
}

std::string messageHandler::handleGetUserList()
{

    std::string reslt = db->getUserList();

    return reslt;
}

std::string messageHandler::handleGetUserFromUserName(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);
    std::string username = v[1];

    std::string reslt = db->getUserFromUserName(username);

    return reslt;
}

std::string messageHandler::handleGetUserFromUserID(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);
    std::string userID = v[1];

    std::string reslt = db->getUserFromUserID(userID);

    return reslt;
}

std::string messageHandler::handleAddUser(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);

    std::string username = v[1], email = v[2], password = v[3];
    bool adminFlag = boost::lexical_cast<bool>(v[4]);

    bool succ = db->addUser(username, email, password, adminFlag);

    if (succ != true) {
        return "INTERNAL_ERROR";
    }

    return "200";
}

std::string messageHandler::handleAddStageComplete(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);
    std::string userID = v[1];
    int stageID = std::stoi(v[2]);

    bool succ = db->addStageComplete(userID, stageID);
    if (succ != true) {
        return "INTERNAL_ERROR";
    }

    return "200";

}

std::string messageHandler::handleGetUserCompleteStages(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);

    std::string userID = v[1];

    std::string reslt = db->getUserCompleteStages(userID);

    return reslt;
}


std::string messageHandler::handleRemoveUser(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);
    std::string userID = v[1];
    bool succ = db->removeUser(userID);
    if (succ != true) {
        return "INTERNAL_ERROR";
    }
    return "200";
}

std::string messageHandler::handleGetStageIDFromName(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);
    try{
        std::string stageName = v[1];
        int id = db->getStageIDFromName(stageName);
        return std::to_string(id);
    }catch(...){
        return "INTERNAL_ERROR";
    }
}

std::string messageHandler::handleGetStage(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);
    int stageID = stoi(v[1]);
    std::string stage = db->getStage(stageID);

    return stage;
}

std::string messageHandler::handleRemoveStage(const std::string &data)
{
    std::vector<std::string> v = getArgs(data);
    int stageID = stoi(v[1]);
    bool succ = db->removeStage(stageID);
    if (succ != true){
        return "INTERNAL_ERROR";
    }
    return "200";
}


std::string messageHandler::resolveMessage(zmq::message_t &message)
{
    std::string data = std::string(static_cast<char *>(message.data()), message.size());
    std::cout << "Recived Message: " << data << std::endl;
    std::string reslt;
    std::vector<std::string> v = getArgs(data);
    if (v[0] == "AddStage")
        reslt = handleAddStage(data);
    else if (v[0] == "UpdateStage")
        reslt = handleUpdateStage(data);
    else if (v[0] == "AddUser")
        reslt = handleAddUser(data);
    else if (v[0] == "GetCurrentUserStage")
        reslt = handleGetCurrentUserStage(data);
    else if (v[0] == "UpdateUser")
        reslt = handleUpdateUser(data);
    else if (v[0] == "GetUserList")
        reslt = handleGetUserList();
    else if (v[0] == "AddStageComplete")
        reslt = handleAddStageComplete(data);
    else if (v[0] == "GetUserCompleteStages")
        reslt = handleGetUserCompleteStages(data);
    else if (v[0] == "GetUserFromUserName")
        reslt = handleGetUserFromUserName(data);
    else if (v[0] == "GetUserFromUserID")
        reslt = handleGetUserFromUserID(data);
    else if (v[0] == "GetStageIDFromName")
        reslt = handleGetStageIDFromName(data);
    else if (v[0] == "RemoveUser")
        reslt = handleRemoveUser(data);
    else if (v[0] == "GetStage")
        reslt = handleGetStage(data);
    else if (v[0] == "RemoveStage")
        reslt = handleRemoveStage(data);
    else
        reslt = "ERROR:UNKNOWN_OPERATION";

    return reslt;
}
