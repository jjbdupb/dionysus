##############################################################################
#  ORIGINAL AUTHOR
#      Joshua du Parc Braham
#
#  DESCRIPTION
#      Controller Provision script
#
##############################################################################

zypper -n install gcc-c++
zypper -n install libpqxx-devel 
zypper -n install libboost_headers1_66_0-devel
zypper -n install fmt-devel
zypper -n install git
zypper -n install cmake
zypper -n install make

git clone https://github.com/zeromq/libzmq.git --depth 1
cd libzmq
mkdir build
cd build
cmake ..
make -j4 install
cd /opt/


git clone https://github.com/zeromq/cppzmq.git --depth 1
cd cppzmq
mkdir build
cd build
cmake ..
make -j4 install
cd /opt/

zypper -n install libzmq-devel

make
make clean

exit 0