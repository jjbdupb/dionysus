#ifndef __DATABASEINTERFACE__
#define __DATABASEINTERFACE__

#include <string>
#include <iostream>
#include <unistd.h>
#include <pqxx/pqxx>
#include <fmt/format.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

/**
* @brief Class containing all database functions
*/
class Database
{
public:
    pqxx::connection *db = nullptr;
    std::string dbName, user, password, host, port, timeout;
    int maxconnect, attempts, waitTime;
    /**
    * @brief Constuctor for database, sets up necesery connections to postgresql database
    * @param conf_file : filepath to config file containing setup information
    */
    Database(std::string conf_file);

    Database(Database &&other);

    Database(const Database &other) = delete;

    Database &operator=(const Database &) = delete;

    ~Database();

    /**
     * @brief Connects to the database
     * @return boolean stating if connection was succsefull or not
     */
    bool connect();

    /**
     * @brief adds a stage to the database
     * @param name : string containing name of stage to add
     * @param description : string containing description of stage to add
     * @param clue : string containing clue information of stage to add
     * @param solution : string containing solution to stage
     * @param longitude : float containing logitude of stage
     * @param latitude : float containing latitude of stage
     * @param cluster : string containing cluster name of stage
     * @return bool of weather add stage was succsefull or not
    */
    bool addStage(std::string name, std::string description, std::string clue, std::string solution, float longitude, float latitude, std::string cluster);

    /**
     * @brief updates a given stage
     * @param stageID : int of stageID of stage to update
     * @param field : string of field to update within stage
     * @param value : string of new value for given feild to take
    */
    bool updateStage(int stageID, std::string field, std::string value);

    /**
     * @brief adds a user to the database
     * @param username : string of username of user
     * @param email : string of email of user
     * @param password : string containing password hash of user
     * @param adminflag : bool stating if the user is an admin or not
     * @return boolean of weather adding user was succsefull or not
    */
    bool addUser(std::string username, std::string email, std::string password, bool adminFlag);

    /**
     * @brief get user information from username
     * @param username : string of the username of a given user
     * @return string containing user information from found user
    */
    std::string getUserFromUserName(std::string username);

    /**
     * @brief get a user information from ID
     * @param userID : string of a users UUID
     * @return string containing user information from found user
    */
    std::string getUserFromUserID(std::string userID);


    /**
     * @brief get a users current stage
     * @param userID : string of a users UUID
     * @return int of stageID of a users current stage
    */
    int getCurrentUserStage(std::string userID);

    /**
     * @brief updates a users information
     * @param userID : string of a users UUID
     * @param field : string of the feild to update within the user
     * @param value : string of new value the given field should take
     * @return boolean of weather the update was succsefull or not
    */
    bool updateUser(std::string userID, std::string field, std::string value);

    /**
     * @brief gets a list of every user and their information
     * @return string of every users information with each user seperated by a |
    */
    std::string getUserList();

    /**
     * @brief gets a stages information
     * @param stageID : int of stageID to get information about
     * @return string containing all stage information found
    */
    std::string getStage(int stageID);

    /**
     * @brief adds a completed stage
     * @param userID : string containing a users UUID
     * @param stageID : int of the stageID that has been completed
     * @return boolean of weather the new entry was succsefull or not
    */
    bool addStageComplete(std::string userID, int stageID);

    /**
     * @brief gets all the completed stages of a given user
     * @param userID : string of a users UUID
     * @return : string containing a list of all completed stages of a user seperated by a |
    */
    std::string getUserCompleteStages(std::string userID);

    /**
     * @brief gets a stageID from StageName
     * @param stageName : string of stage's name to get ID of
     * @return int of a found stages ID
    */
    int getStageIDFromName(std::string stageName);

    /**
     * @brief removes a user from the database
     * @param userID : the UUID of the user to remove from the database
     * @return boolean of weather removing the user was succsefull or not
    */
    bool removeUser(std::string userID);

    /**
     * @breif removes a stage from the database
     * @param stageID: the UUID of the stage to remove
     * @return boolean of weather removing the stage was succesfull or not
    */
    bool removeStage(int stageID);


};

#endif